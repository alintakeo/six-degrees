//
//  MainViewController.swift
//  SixDegrees
//
//  Created by Alin Nomura on 1/16/16.
//  Copyright © 2016 Nomura. All rights reserved.
//

import UIKit
import StoreKit

protocol MenuActionDelegate {
    func resetMenu()
    func considerAskingUserToRate()
}

class MainViewController: UIViewController, SearchViewDelegate, SKProductsRequestDelegate, SKPaymentTransactionObserver, MenuActionDelegate {

    var productsRequest:SKProductsRequest?
    var product:SKProduct?
    var userIsWaitingOnProductInfo = false
    var message:MessageBox?

    @IBOutlet var sceneView: MenuScene!
    @IBOutlet var blurEffectView: UIVisualEffectView!
    @IBOutlet var playButton: UIButton!
    @IBOutlet var xButton: UIButton!
    @IBOutlet var startActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var startImageView: UIImageView!
    @IBOutlet var startPersonButton: UIButton!
    @IBOutlet var endActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var endImageView: UIImageView!
    @IBOutlet var endPersonButton: UIButton!
    @IBOutlet var startButton: UIButton!
    @IBOutlet var footerView: UIView!
    @IBOutlet var tmdbImageView: UIImageView!
    @IBOutlet var helpButton: UIButton!
    // Constraints
    @IBOutlet var playCenterY: NSLayoutConstraint!
    @IBOutlet var footerBottomSpace: NSLayoutConstraint!
    @IBOutlet var startImageViewHeight: NSLayoutConstraint!
    @IBOutlet var endImageViewHeight: NSLayoutConstraint!
    @IBOutlet var startImageToButtonSpace: NSLayoutConstraint!
    @IBOutlet var endImageToButtonSpace: NSLayoutConstraint!
    @IBOutlet var startButtonBottomSpace: NSLayoutConstraint!
    @IBOutlet var startButtonWidth: NSLayoutConstraint!
    @IBOutlet var endButtonWidth: NSLayoutConstraint!
    
    @IBAction func titleTapped() {
        self.sceneView.reset()
    }
    @IBAction func playTapped() {
        // If both start and end people are not random and start and end people are the
        // same then return with a message to the user that this is not allowed
        if (!BL.getStartPersonRandom() && !BL.getEndPersonRandom() && (BL.getStartPerson()!.id == BL.getEndPerson()!.id)) {
            self.message = MessageBox()
            self.message!.title = "Matched Celebrities"
            self.message!.message = "The starting and ending celebrities may not be the same. Please change one of the selected celebrities."
            self.message!.button1Title = kOK
            self.message!.button1DoesDismiss = true
            self.message!.show(controller: self)
            return
        }
        
        var startPerson:Person!
        if BL.getStartPersonRandom() {
            if BL.getEndPersonRandom() {
                startPerson = BL.getRandomPerson(otherPerson:nil)
            } else {
                startPerson = BL.getRandomPerson(otherPerson:BL.getEndPerson()!)
            }
        } else {
            startPerson = BL.getStartPerson()!
        }
        BL.setFirstPerson(startPerson)
        var endPerson:Person!
        if BL.getEndPersonRandom() {
            endPerson = BL.getRandomPerson(otherPerson: startPerson)
        } else {
            endPerson = BL.getEndPerson()
        }
        BL.setLastPerson(endPerson)
        
        BL.addProfileImageWithPath(startPerson.profilePath, imageView: self.startImageView, activityIndicator: self.startActivityIndicator)
        BL.addProfileImageWithPath(endPerson.profilePath, imageView: self.endImageView, activityIndicator: self.endActivityIndicator)
        self.endPersonButton.setTitle(endPerson.name, for: UIControlState.normal)
        self.startPersonButton.isUserInteractionEnabled = false
        self.endPersonButton.isUserInteractionEnabled = false
        var imageHeight:CGFloat = 200
        let screenHeight = UIScreen.main.nativeBounds.height
        if screenHeight < 1334 { // Shorter than iPhone 6
            imageHeight = 150
        }
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.blurEffectView.effect = UIBlurEffect(style: .dark)
            self.playCenterY.constant = -((self.view.frame.height + self.playButton.frame.height)/2 + 8)
            self.startImageToButtonSpace.constant = 8
            self.footerBottomSpace.constant = -self.footerView.frame.height
            self.endImageToButtonSpace.constant = 8
            self.startImageViewHeight.constant = imageHeight
            self.endImageViewHeight.constant = imageHeight
            self.view.layoutIfNeeded()
        }, completion: { (finished:Bool) in
            self.sceneView.destroyGraph()
            if BL.getStartPersonRandom() {
                self.startPersonButton.alpha = 1
                self.startPersonButton.setTitle(startPerson.name, for: UIControlState.normal)
            }
            if BL.getEndPersonRandom() {
                self.endPersonButton.alpha = 1
                self.endPersonButton.setTitle(endPerson.name, for: UIControlState.normal)
            }
            UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.xButton.alpha = 1
                self.startImageView.alpha = 1
                self.endImageView.alpha = 1
                self.startButtonBottomSpace.constant = 8
                self.startButtonWidth.constant = self.view.frame.width + 50
                self.endButtonWidth.constant = self.view.frame.width + 50
                self.startPersonButton.backgroundColor = UIColor.black.withAlphaComponent(0.3)
                self.endPersonButton.backgroundColor = UIColor.black.withAlphaComponent(0.3)
                self.view.layoutIfNeeded()
            }, completion: { (finished:Bool) in
                self.xButton.isUserInteractionEnabled = true
            })
        })
    }
    @IBAction func xButtonTapped() {
        self.resetWithAnimation()
    }
    @IBAction func startPersonButtonTapped(_ sender: UIButton) {
        if BL.ableToSelectPeople() {
            self.selectPerson(isStartPerson: true)
        } else {
            self.promptIAPMessage()
        }
    }
    @IBAction func endPersonButtonTapped(_ sender: UIButton) {
        if BL.ableToSelectPeople() {
            self.selectPerson(isStartPerson: false)
        } else {
            self.promptIAPMessage()
        }
    }
    @IBAction func startButtonTapped() {
        if BL.getFirstPerson() != nil {
            let playViewController:PlayViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PlayViewControllerID") as! PlayViewController
            playViewController.modalPresentationStyle = UIModalPresentationStyle.custom
            playViewController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            playViewController.menuActionDelegate = self
            let imageFrameInTopView = self.startImageView.superview!.convert(self.startImageView.frame, to: self.view)
            playViewController.initialStartFrame = imageFrameInTopView
            playViewController.endPersonImage = self.endImageView.image
            self.present(playViewController, animated: true, completion: nil)
        }
    }
    @IBAction func tmdbButtonTapped() {
        self.message = MessageBox()
        self.message!.title = "TMDb"
        self.message!.message = "This product uses the TMDb API but is not endorsed or certified by TMDb."
        self.message!.button1Title = kOK
        self.message!.button1DoesDismiss = true
        self.message!.titleSelector = #selector(MainViewController.openTmdbSite)
        self.message!.show(controller: self)
    }
    func openTmdbSite() {
        let url = URL(string: "https://www.themoviedb.org/")!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    @IBAction func helpButtonTapped() {
        self.message = MessageBox()
        self.message!.title = "How to Play"
        self.message!.message = "The object of SIX° is to link two actors together in six steps or less by associating a sequence of actor filmographies. Each step from one celebrity to another is considered a degree of separation (hence the name).\n\nThe game starts by showing you the two celebrities that you must link together. You are then presented with the starting actor's filmography. When you select one of these films its cast list will be displayed. Then you select one of those actors and the process continues until you reach the ending celebrity. It usually takes a few steps to link them together.\n\nFor instance, if you wanted to link Val Kilmer to Kevin Bacon you could do it in two steps because Val Kilmer was in Top Gun with Tom Cruise and Tom Cruise was in A Few Good Men with Kevin Bacon. In the game you would select Top Gun from Val Kilmer's filmography, then select Tom Cruise from Top Gun's cast list, then select A Few Good Men from Tom's list of films, and finally you would select Kevin Bacon from that cast list."
        self.message!.button1Title = kOK
        self.message!.button1DoesDismiss = true
        self.message!.show(controller: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.product == nil {
            self.requestStoreProducts()
        }
        self.blurEffectView.effect = nil
        self.playButton.layer.cornerRadius = self.playButton.frame.height/2
        self.startButton.layer.cornerRadius = self.startButton.frame.height/2
        self.startPersonButton.layer.borderWidth = 1
        self.startPersonButton.layer.borderColor = UI.MainBlue().cgColor
        self.startPersonButton.layer.cornerRadius = self.startPersonButton.frame.height/2
        self.endPersonButton.layer.borderWidth = 1
        self.endPersonButton.layer.borderColor = UI.MainBlue().cgColor
        self.endPersonButton.layer.cornerRadius = self.endPersonButton.frame.height/2
        self.tmdbImageView.tintColor = UIColor.white
        self.helpButton.layer.borderWidth = 1
        self.helpButton.layer.borderColor = UI.MainBlue().cgColor
        self.helpButton.layer.cornerRadius = self.helpButton.frame.height/2
        self.refresh()
    }

    func refresh() {
        if BL.getStartPersonRandom() {
            self.startPersonButton.setTitle(RANDOM, for: UIControlState.normal)
        } else if let startName = BL.getStartPerson()?.name {
            self.startPersonButton.setTitle(startName, for: UIControlState.normal)
        }
        if BL.getEndPersonRandom() {
            self.endPersonButton.setTitle(RANDOM, for: UIControlState.normal)
        } else if let endName = BL.getEndPerson()?.name {
            self.endPersonButton.setTitle(endName, for: UIControlState.normal)
        }
    }
    
    private func selectPerson(isStartPerson:Bool) {
        let title = isStartPerson ? "Select a Starting Celebrity" : "Select an Ending Celebrity"
        let searchView = SearchView(frame: self.view.bounds, title:title)
        searchView.delegate = self
        searchView.isStartPerson = isStartPerson
        self.view.addSubview(searchView)
    }
    
    func resetWithAnimation() {
        self.xButton.isUserInteractionEnabled = false
        if BL.getStartPersonRandom() {
            self.startPersonButton.setTitle(RANDOM, for: UIControlState.normal)
        } else {
            self.startPersonButton.setTitle(BL.getStartPerson()!.name, for: UIControlState.normal)
        }
        if BL.getEndPersonRandom() {
            self.endPersonButton.setTitle(RANDOM, for: UIControlState.normal)
        } else {
            self.endPersonButton.setTitle(BL.getEndPerson()!.name, for: UIControlState.normal)
        }
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.xButton.alpha = 0
            self.startImageView.alpha = 0
            self.endImageView.alpha = 0
            self.startButtonBottomSpace.constant = -self.startButton.frame.height
            self.startButtonWidth.constant = 300
            self.endButtonWidth.constant = 300
            self.startPersonButton.backgroundColor = UIColor.clear
            self.endPersonButton.backgroundColor = UIColor.clear
            self.view.layoutIfNeeded()
        }, completion: { (finished:Bool) in
            UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.blurEffectView.effect = nil
                self.xButton.alpha = 0
                self.playCenterY.constant = 0
                self.startImageToButtonSpace.constant = 0
                self.footerBottomSpace.constant = 0
                self.endImageToButtonSpace.constant = 0
                self.startImageViewHeight.constant = 0
                self.endImageViewHeight.constant = 0
                self.view.layoutIfNeeded()
            }, completion: { (finished:Bool) in
                self.startImageView.image = nil
                self.endImageView.image = nil
                self.startPersonButton.isUserInteractionEnabled = true
                self.endPersonButton.isUserInteractionEnabled = true
                self.sceneView.reset()
            })
        })
    }
    
    // MARK: MenuActionDelegate
    // Protocol method that duplicates functionality of reset() but without the animations
    func resetMenu() {
        self.xButton.isUserInteractionEnabled = false
        if BL.getStartPersonRandom() {
            self.startPersonButton.setTitle(RANDOM, for: UIControlState.normal)
        } else {
            self.startPersonButton.setTitle(BL.getStartPerson()!.name, for: UIControlState.normal)
        }
        if BL.getEndPersonRandom() {
            self.endPersonButton.setTitle(RANDOM, for: UIControlState.normal)
        } else {
            self.endPersonButton.setTitle(BL.getEndPerson()!.name, for: UIControlState.normal)
        }
        self.blurEffectView.effect = nil
        self.xButton.alpha = 0
        self.startImageView.alpha = 0
        self.endImageView.alpha = 0
        self.startButtonBottomSpace.constant = -self.startButton.frame.height
        self.startButtonWidth.constant = 300
        self.endButtonWidth.constant = 300
        self.startPersonButton.backgroundColor = UIColor.clear
        self.endPersonButton.backgroundColor = UIColor.clear
        self.xButton.alpha = 0
        self.playCenterY.constant = 0
        self.startImageToButtonSpace.constant = 0
        self.footerBottomSpace.constant = 0
        self.endImageToButtonSpace.constant = 0
        self.startImageViewHeight.constant = 0
        self.endImageViewHeight.constant = 0
        self.startImageView.image = nil
        self.endImageView.image = nil
        self.startPersonButton.isUserInteractionEnabled = true
        self.endPersonButton.isUserInteractionEnabled = true
        self.sceneView.reset()
        self.view.layoutIfNeeded()
    }
    
    // Ask user to rate the App
    func considerAskingUserToRate() {
        // Only ask the user to rate the app if they haven't been asked before and they have won 3 times
        if !BL.getHasBeenAskedToRate() && BL.getNumberOfWins() == 3 {
            self.message = MessageBox()
            self.message!.title = "Rate Six°"
            self.message!.message = "If you have enjoyed this app please consider rating it on the App Store. Sorry to bother you. I won't bug you about this again :)"
            self.message!.button1Title = "No Thanks"
            self.message!.button1DoesDismiss = true
            self.message!.button2Title = "Rate Now"
            self.message!.button2Selector = #selector(MainViewController.rateNow)
            self.message!.button2DoesDismiss = true
            self.message!.show(controller: self)
            BL.askedUserToRate()
        }
    }
    
    func rateNow() {
        if let url = URL(string : "itms-apps://itunes.apple.com/app/1272433282") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    // MARK: SearchViewDelegate
    func selectedPerson(_ person: Person, isStartPerson:Bool) {
        if isStartPerson {
            BL.setStartPerson(person)
        } else {
            BL.setEndPerson(person)
        }
        self.refresh()
    }
    func selectedRandom(isStartPerson:Bool) {
        if isStartPerson {
            BL.setStartPersonRandom(true)
        } else {
            BL.setEndPersonRandom(true)
        }
        self.refresh()
    }
    func presentAlert(alert: UIAlertController) {
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: In-App Purchase
    func requestStoreProducts() {
        let idSet:Set<String> = [kProductID]
        self.productsRequest = SKProductsRequest(productIdentifiers:idSet)
        self.productsRequest!.delegate = self
        self.productsRequest!.start()
    }
    
    private func promptIAPMessage() {
        if let product = self.product {
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            formatter.locale = product.priceLocale
            let localizedPrice = formatter.string(from: product.price)!

            self.message = MessageBox()
            self.message!.title = "Unlock Full Game"
            self.message!.message = "In the basic version, the challenge is to link random movie stars to Kevin Bacon. Upgrade to unlock the ability to choose your starting and ending celebrities."
            self.message!.button1Title = "Restore"
            self.message!.button1Selector = #selector(MainViewController.restorePurchases)
            self.message!.button1DoesDismiss = true
            self.message!.button2Title = "Purchase (\(localizedPrice))"
            self.message!.button2Selector = #selector(MainViewController.makePurchase)
            self.message!.button2DoesDismiss = true
            self.message!.hasDismissButton = true
            self.message!.show(controller: self)
        } else {
            self.userIsWaitingOnProductInfo = true
            self.requestStoreProducts()
        }
    }
    
    func makePurchase() {
        if SKPaymentQueue.canMakePayments() {
            if let product = self.product {
                SKPaymentQueue.default().add(self)
                let payment:SKPayment = SKPayment(product: product)
                SKPaymentQueue.default().add(payment)
            }
        } else {
            self.showUnauthorizedMessage()
        }
    }
    
    func restorePurchases() {
        if SKPaymentQueue.canMakePayments() {
            SKPaymentQueue.default().add(self)
            SKPaymentQueue.default().restoreCompletedTransactions()
        } else {
            self.showUnauthorizedMessage()
        }
    }
    
    func showUnauthorizedMessage() {
        self.message = MessageBox()
        self.message!.title = "Unable to Make Purchase"
        self.message!.message = "Purchase Not Allowed. You are not authorized to make purchases at this time. This may be due to purchase restriction settings."
        self.message!.button1Title = kOK
        self.message!.button1DoesDismiss = true
        self.message!.show(controller: self)
    }
    
    // MARK: SKProductsRequestDelegate
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        for product in response.products {
            if product.productIdentifier == kProductID {
                self.product = product
            }
        }
        if self.userIsWaitingOnProductInfo {
            self.userIsWaitingOnProductInfo = false
            self.promptIAPMessage()
        }
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        if self.userIsWaitingOnProductInfo {
            self.message = MessageBox()
            self.message!.title = "Error"
            self.message!.message = error.localizedDescription
            self.message!.button1Title = "Retry"
            self.message!.button1Selector = #selector(MainViewController.requestStoreProducts)
            self.message!.button1DoesDismiss = true
            self.message!.hasDismissButton = true
            self.message!.show(controller: self)
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchasing:
                print("Transaction Purchasing...")
            case .purchased:
                self.transactionStatePurchased(transaction)
            case .failed:
                self.transactionStateFailed(transaction)
            case .restored:
                self.transactionStateRestored(transaction)
            case .deferred:
                print("Transaction Deferred...")
            }
        }
    }
    
    private func transactionStatePurchased(_ transaction:SKPaymentTransaction) {
        BL.successfullyMadePurchase()
        self.message = MessageBox()
        self.message!.title = "Purchase Successful"
        self.message!.message = "You may now choose the starting and ending celebrities!"
        self.message!.button1Title = kOK
        self.message!.button1DoesDismiss = true
        self.message!.show(controller: self)
        SKPaymentQueue.default().finishTransaction(transaction)
    }
    
    private func transactionStateFailed(_ transaction:SKPaymentTransaction) {
        if let transactionError = transaction.error as NSError? {
            if transactionError.code != SKError.paymentCancelled.rawValue {
                self.message = MessageBox()
                self.message!.title = "Purchase Unsuccessful"
                self.message!.message = "Your purchase transaction failed. Please try again."
                self.message!.button1Title = kOK
                self.message!.button1DoesDismiss = true
                self.message!.show(controller: self)
            }
        }
        SKPaymentQueue.default().finishTransaction(transaction)
    }
    
    private func transactionStateRestored(_ transaction:SKPaymentTransaction) {
        BL.successfullyMadePurchase()
        self.message = MessageBox()
        self.message!.title = "Restore Successful"
        self.message!.message = "You may now choose the starting and ending celebrities!"
        self.message!.button1Title = kOK
        self.message!.button1DoesDismiss = true
        self.message!.show(controller: self)
        SKPaymentQueue.default().finishTransaction(transaction)
    }
}
