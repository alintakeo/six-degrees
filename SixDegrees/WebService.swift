//
//  WebService.swift
//  SixDegrees
//
//  Created by Alin Nomura on 1/16/16.
//  Copyright © 2016 Nomura. All rights reserved.
//

import Foundation

import UIKit

class WebService {
    static let shared = WebService()
    
    fileprivate let baseUrl:String = "https://api.themoviedb.org/3/"
    fileprivate let apiParameters:[String] = ["api_key=dcdec012333250c1bf0eb35a8b832559"]
    
    func getDataForResource(_ resource: String, params:[String], completion: @escaping (_ success: Bool, _ jsonObject: Any?, _ errorMessage:String?) -> ()) {
        let allParams:[String] = apiParameters + params
        let joinedParams = allParams.joined(separator: "&")
        if let escapedString = (baseUrl + resource + "?" + joinedParams).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
            let url = URL(string: escapedString)
            let request = URLRequest(url: url!)
            let session = URLSession.shared
            let task = session.dataTask(with: request) { (data:Data?, response:URLResponse?, error:Error?) -> Void in
                if let data = data {
                    let json = try? JSONSerialization.jsonObject(with: data, options: [])
                    if let response = response as? HTTPURLResponse {
                        if 200...299 ~= response.statusCode {
                            completion(true, json, nil)
                        } else {
                            if let json = json as? [String:Any], let message = json["status_message"] as? String {
                                completion(false, json, message)
                            } else {
                                completion(false, json, nil)
                            }
                        }
                    } else {
                        completion(false, json, nil)
                    }
                } else {
                    completion(false, nil, nil);
                }
            }
            task.resume()
        }
    }
}



