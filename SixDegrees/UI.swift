//
//  UI.swift
//  SixDegrees
//
//  Created by Alin Nomura on 11/21/16.
//  Copyright © 2016 Nomura. All rights reserved.
//

import UIKit

class UI {
    class func DarkColor() -> UIColor {
        return UIColor(colorLiteralRed: 0.1, green: 0.1, blue: 0.1, alpha: 1.0)
    }
    class func LightColor() -> UIColor {
        return UIColor.white
    }
    class func MainBlue() -> UIColor {
        return UIColor(red: 0, green: 0.71, blue: 0.8, alpha: 1)
    }
    class func MainDarkBlue() -> UIColor {
        return UIColor(red: 0, green: 0.44, blue: 0.5, alpha: 1)
    }
}
