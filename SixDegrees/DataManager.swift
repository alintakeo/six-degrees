//
//  DataManager.swift
//  SixDegrees
//
//  Created by Alin Nomura on 1/16/16.
//  Copyright © 2016 Nomura. All rights reserved.
//

import Foundation

class DataManager {
    static let shared = DataManager()
    
    var firstPerson:Person?
    var lastPerson:Person?

    let userDefaults = UserDefaults.standard
    var selectedPerson:Person? = nil
    var selectedMedia:Media? = nil
    
    // MARK: User Defaults
    enum UserDefaultKey:String {
        case ImageBaseUrlKey = "ImageBaseUrlKey"
        case PosterSizeMedKey = "PosterSizeMedKey"
        case ConfigSetKey = "ConfigSetKey"
        case StartPeopleKey = "StartPeopleKey"
        case StartPersonKey = "StartPersonKey"
        case StartPersonIsRandomKey = "StartPersonIsRandomKey"
        case EndPersonKey = "EndPersonKey"
        case EndPersonIsRandomKey = "EndPersonIsRandomKey"
        case AbleToSelectPeopleKey = "AbleToSelectPeopleKey"
        case NumberOfWinsKey = "NumberOfWinsKey"
        case HasBeenAskedToRateKey = "HasBeenAskedToRateKey"
    }
    
    // MARK: Image base URL
    func getImageBaseUrl() -> String {
        if let value = self.userDefaults.object(forKey: UserDefaultKey.ImageBaseUrlKey.rawValue) as? String {
            return value
        } else {
            return "http://image.tmdb.org/t/p/"
        }
    }
    
    func setImageBaseUrl(_ value:String) {
        self.userDefaults.set(value, forKey: UserDefaultKey.ImageBaseUrlKey.rawValue)
    }
    
    // MARK: Poster Size
    func getPosterSizeMedium() -> String {
        if let value = self.userDefaults.object(forKey: UserDefaultKey.PosterSizeMedKey.rawValue) as? String {
            return value
        } else {
            return "w185"
        }
    }
    
    func setPosterSizeMedium(_ value:String) {
        self.userDefaults.set(value, forKey: UserDefaultKey.PosterSizeMedKey.rawValue)
    }
    
    func getConfigurationIsSet() -> Bool {
        if let value = self.userDefaults.object(forKey: UserDefaultKey.ConfigSetKey.rawValue) as? Bool {
            return value
        } else {
            return false
        }
    }
    
    func setConfigurationIsSet(_ value:Bool) {
        self.userDefaults.set(value, forKey: UserDefaultKey.ConfigSetKey.rawValue)
    }
    
    func getStartPeopleData() -> [[String:AnyObject]] {
        if let value = self.userDefaults.object(forKey: UserDefaultKey.StartPeopleKey.rawValue) as? [[String:AnyObject]] {
            return value
        }
        return []
    }
    
    func setStartPeopleData(_ value:[[String:AnyObject]]) {
        self.userDefaults.setValue(value, forKey: UserDefaultKey.StartPeopleKey.rawValue)
    }
    
    // MARK: Start Person
    func getStartPersonData() -> [String:AnyObject]? {
        return self.userDefaults.object(forKey: UserDefaultKey.StartPersonKey.rawValue) as? [String:AnyObject]
    }
    func setStartPersonData(_ value:[String:AnyObject]) {
        self.userDefaults.setValue(value, forKey: UserDefaultKey.StartPersonKey.rawValue)
    }
    func getStartPersonIsRandom() -> Bool {
        return self.userDefaults.bool(forKey: UserDefaultKey.StartPersonIsRandomKey.rawValue)
    }
    func setStartPersonIsRandom(_ value:Bool) {
        self.userDefaults.set(value, forKey: UserDefaultKey.StartPersonIsRandomKey.rawValue)
    }
    
    // MARK: End Person
    func getEndPersonData() -> [String:AnyObject]? {
        return self.userDefaults.object(forKey: UserDefaultKey.EndPersonKey.rawValue) as? [String:AnyObject]
    }
    func setEndPersonData(_ value:[String:AnyObject]) {
        self.userDefaults.setValue(value, forKey: UserDefaultKey.EndPersonKey.rawValue)
    }
    func getEndPersonIsRandom() -> Bool {
        return self.userDefaults.bool(forKey: UserDefaultKey.EndPersonIsRandomKey.rawValue)
    }
    func setEndPersonIsRandom(_ value:Bool) {
        self.userDefaults.set(value, forKey: UserDefaultKey.EndPersonIsRandomKey.rawValue)
    }
    
    // In-App Purchase
    func getAbleToSelectPeople() -> Bool {
        return self.userDefaults.bool(forKey: UserDefaultKey.AbleToSelectPeopleKey.rawValue)
    }
    func setAbleToSelectPeople(_ value:Bool) {
        self.userDefaults.set(value, forKey: UserDefaultKey.AbleToSelectPeopleKey.rawValue)
    }
    
    // Data for asking user to rate app
    func getNumberOfWins() -> Int {
        return self.userDefaults.integer(forKey: UserDefaultKey.NumberOfWinsKey.rawValue)
    }
    func setNumberOfWins(_ value:Int) {
        self.userDefaults.set(value, forKey: UserDefaultKey.NumberOfWinsKey.rawValue)
    }
    
    func getHasBeenAskedToRate() -> Bool {
        return self.userDefaults.bool(forKey: UserDefaultKey.HasBeenAskedToRateKey.rawValue)
    }
    func setHasBeenAskedToRate(_ value:Bool) {
        self.userDefaults.set(value, forKey: UserDefaultKey.HasBeenAskedToRateKey.rawValue)
    }
}
