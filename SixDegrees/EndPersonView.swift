//
//  EndPersonView.swift
//  SixDegrees
//
//  Created by Alin Nomura on 8/20/17.
//  Copyright © 2017 Nomura. All rights reserved.
//

import UIKit

protocol EndPersonViewDelegate {
    func hideEndPersonView()
}

class EndPersonView: UIView {
    
    var delegate:EndPersonViewDelegate?
    @IBOutlet var header: UILabel!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var okButton: UIButton!
   
    @IBAction func okButtonTapped() {
        self.delegate?.hideEndPersonView()
    }

    override init(frame:CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupView()
    }
    
    fileprivate func setupView() {
        let view = viewFromNibForClass()
        view.frame = bounds
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        addSubview(view)
        
        view.layer.borderWidth = 1
        view.layer.borderColor = UI.MainBlue().cgColor
        view.layer.cornerRadius = self.okButton.frame.height / 2
        
        self.header.layer.borderWidth = 1
        self.header.layer.borderColor = UI.MainBlue().cgColor
        
        self.okButton.layer.cornerRadius = self.okButton.frame.height / 2
    }
   
    fileprivate func viewFromNibForClass() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
}


