//
//  SearchView.swift
//  SixDegrees
//
//  Created by Alin Nomura on 8/11/17.
//  Copyright © 2017 Nomura. All rights reserved.
//

import UIKit

protocol SearchViewDelegate {
    func selectedPerson(_ person:Person, isStartPerson:Bool)
    func selectedRandom(isStartPerson:Bool)
    func presentAlert(alert:UIAlertController)
}

class SearchView: UIView, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate {
    
    var delegate:SearchViewDelegate?
    var foundPeople:[Person] = []
    var isStartPerson:Bool!
    var dismissingSearchView = false
    
    @IBOutlet var blurView: UIVisualEffectView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var header: UILabel!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var noResultsLabel: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var randomButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
    // Constraints
    @IBOutlet var viewMidConstraint: NSLayoutConstraint!
    @IBOutlet var tableHeightConstraint: NSLayoutConstraint!
    
    @IBAction func randomButtonTapped() {
        if let delegate = self.delegate {
            delegate.selectedRandom(isStartPerson:self.isStartPerson)
            self.dismissView()
        }
    }
    @IBAction func cancelButtonTapped() {
        self.dismissView()
    }
    
    init(frame:CGRect, title:String) {
        super.init(frame: frame)
        self.setupView(title)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupView("")
    }
    
    fileprivate func setupView(_ title:String) {
        let view = viewFromNibForClass()
        view.frame = bounds
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        addSubview(view)
        
        NotificationCenter.default.addObserver(self, selector: #selector(SearchView.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)

        self.blurView.effect = nil
        
        self.mainView.layer.borderWidth = 1
        self.mainView.layer.cornerRadius = self.randomButton.frame.height / 2
        self.mainView.layer.borderColor = UI.MainBlue().cgColor
        self.mainView.alpha = 0
        
        UIView.animate(withDuration: 0.5) {
            self.blurView.effect = UIBlurEffect(style: .dark)
            self.mainView.alpha = 1
        }
        
        self.header.layer.borderWidth = 1
        self.header.layer.borderColor = UI.MainBlue().cgColor
        self.header.text = title
        
        // Recursively iterate through subviews of the given UIView to find a UITextfield and update its properties
        func findAndUpdateTextfield(_ view:UIView) {
            if let view = view as? UITextField {
                view.font = UIFont(name: kFontName, size: 16)
                view.textColor = UI.MainBlue()
            } else {
                for subview in view.subviews {
                    findAndUpdateTextfield(subview)
                }
            }
        }
        findAndUpdateTextfield(self.searchBar)
        
        self.searchBar.delegate = self
        self.searchBar.becomeFirstResponder()
        
        self.randomButton.layer.cornerRadius = self.randomButton.frame.height / 2
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.rowHeight = 44
        
        self.cancelButton.layer.cornerRadius = self.cancelButton.frame.width/2
        self.cancelButton.layer.borderWidth = 1
        self.cancelButton.layer.borderColor = UI.MainBlue().cgColor
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            self.layoutIfNeeded()
            UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.tableHeightConstraint.constant = 0
                self.viewMidConstraint.constant = (self.frame.height - keyboardHeight) / 2
                self.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    private func openTable() {
        self.layoutIfNeeded()
        
        if self.foundPeople.count < 1 {
            // If found people is 0 then open the table view to reveal the "No Results" label
            UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.tableHeightConstraint.constant = self.noResultsLabel.frame.height
                self.viewMidConstraint.constant = self.frame.height / 2
                self.layoutIfNeeded()
            }, completion: { (finished:Bool) in
                UIView.animate(withDuration: 0.1, animations: { 
                    self.noResultsLabel.alpha = 1
                })
            })
        } else {
            let tableHeight:CGFloat = self.tableView.rowHeight * CGFloat(self.foundPeople.count)
            self.noResultsLabel.alpha = 0
            UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.tableHeightConstraint.constant = tableHeight
                self.viewMidConstraint.constant = self.frame.height / 2
                self.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    fileprivate func viewFromNibForClass() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    // MARK: Table View Data Source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.foundPeople.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let foundPerson = self.foundPeople[indexPath.row]
        let cellId = "FoundPersonCell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellId) {
            return addPersonDataToCell(person: foundPerson, cell: cell)
        }
        let cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: cellId)
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.font = UIFont(name: kFontBoldName, size: 14)
        cell.detailTextLabel?.textColor = UI.MainBlue()
        cell.detailTextLabel?.font = UIFont(name: kFontName, size: 12)
        return addPersonDataToCell(person: foundPerson, cell: cell)
    }
    
    private func addPersonDataToCell(person:Person, cell:UITableViewCell) -> UITableViewCell {
        cell.textLabel?.text = person.name
        if let knownFor = person.knownFor {
            cell.detailTextLabel?.text = knownFor
        }
        return cell
    }
    
    // MARK: Table View Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let foundPerson = self.foundPeople[indexPath.row]
        if let delegate = self.delegate {
            delegate.selectedPerson(foundPerson, isStartPerson:self.isStartPerson)
            self.dismissView()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let searchText = searchBar.text, searchBar.text != "" {
            let activityIndicator = UIActivityIndicatorView(frame: self.searchBar.bounds)
            activityIndicator.activityIndicatorViewStyle = .white
            activityIndicator.startAnimating()
            self.searchBar.addSubview(activityIndicator)
            BL.searchForPeople(searchText) { (success:Bool, people:[Person]?, errorMessage:String?) in
                activityIndicator.removeFromSuperview()
                if !success {
                    if let delegate = self.delegate {
                        var message = kInterwebsFailedMessage
                        if let error = errorMessage {
                            message = "Server Error: \(error)"
                        }
                        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: kOK, style: .cancel, handler: nil))
                        delegate.presentAlert(alert: alert)
                    }
                } else if let people = people {
                    self.foundPeople = people
                    self.tableView.reloadData()
                } else {
                    self.foundPeople = []
                    self.tableView.reloadData()
                }
                self.openTable()
            }
        }
        searchBar.resignFirstResponder()
    }
    
    private func dismissView() {
        self.dismissingSearchView = true
        self.searchBar.resignFirstResponder()
        self.layoutIfNeeded()
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
            self.viewMidConstraint.constant = 0
            self.alpha = 0
            self.layoutIfNeeded()
        }) { (finished:Bool) in
            self.removeFromSuperview()
        }
    }
}


