//
//  BL.swift
//  SixDegrees
//
//  Created by Alin Nomura on 1/16/16.
//  Copyright © 2016 Nomura. All rights reserved.
//

import UIKit

let kKevinBaconID = 4724
let kKevinBaconName = "Kevin Bacon"
let kKevinBaconProfilePath = "/p1uCaOjxSC1xS5TgmD4uloAkbLd.jpg"
let RANDOM = "RANDOM"
let UNKNOWN = "Unknown"

let kFontName = "AvenirNext-Italic"
let kFontBoldName = "AvenirNext-BoldItalic"
let kDismissXFontName = "GilSans-Light"
let kOK = "OK"
let kProductID = "com.Nomura.SixDegrees.UnlockPersonSelection"

let kInterwebsFailedMessage = "Unable to connect. Please check your network."

class BL {
    
    class func appDidLaunch(_ completion:@escaping (Bool, String?) -> Void) {
        // Check if the configuration data is set
        if DataManager.shared.getConfigurationIsSet() == false {
            BL.getConfigurationInfo({ (success:Bool, errorMessage:String?) -> Void in
                completion(success, errorMessage)
            })
        }
        
        // If there is no data in the start people data array
        // then it is the first launch so populate the array
        if DataManager.shared.getStartPeopleData().count == 0 {
            var people:[[String:AnyObject]] = []
            people.append(["id":4566 as AnyObject, "name":"Alan Rickman" as AnyObject, "profilePath":"/q7cZwS6CrGKegJltNwaBY5ZaWCK.jpg" as AnyObject])
            people.append(["id":529 as AnyObject, "name":"Guy Pearce" as AnyObject, "profilePath":"/qpcel7RjjJyiayVmw9zQ389qqwX.jpg" as AnyObject])
            people.append(["id":118 as AnyObject, "name":"Geoffrey Rush" as AnyObject, "profilePath":"/c0jbNjWb9DHm5xfBIeEtHZdZJmI.jpg" as AnyObject])
            people.append(["id":7499 as AnyObject, "name":"Jared Leto" as AnyObject, "profilePath":"/msugySeTCyCmlRWtyB6sMixTQYY.jpg" as AnyObject])
            people.append(["id":6194 as AnyObject, "name":"Claire Danes" as AnyObject, "profilePath":"/mIwLwf04pjpLjV01FUrpPtqiocn.jpg" as AnyObject])
            people.append(["id":83586 as AnyObject, "name":"Ken Jeong" as AnyObject, "profilePath":"/rEebZHRju1WtSOdvQJB5je5ZNGj.jpg" as AnyObject])
            people.append(["id":1267329 as AnyObject, "name":"Lupita Nyong'o" as AnyObject, "profilePath":"/y3p0OMzIuAmcNt700RayK5A0k6t.jpg" as AnyObject])
            people.append(["id":17605 as AnyObject, "name":"Idris Elba" as AnyObject, "profilePath":"/4fuJRkT2TdfznRONUvQmdcayTTx.jpg" as AnyObject])
            people.append(["id":8170 as AnyObject, "name":"Eva Mendes" as AnyObject, "profilePath":"/6JoVMpn0CcZwb6JK7XW2E6ntU52.jpg" as AnyObject])
            people.append(["id":12214 as AnyObject, "name":"Gillian Anderson" as AnyObject, "profilePath":"/uXw82aq2bH7GUYQIaoihXwDolLK.jpg" as AnyObject])
            people.append(["id":514 as AnyObject, "name":"Jack Nicholson" as AnyObject, "profilePath":"/hINAkm21g80UbaAxA6rHhOaT5Jk.jpg" as AnyObject])
            people.append(["id":70851 as AnyObject, "name":"Jack Black" as AnyObject, "profilePath":"/kAyKg3rYGgIhB5KRaIWALuf78W3.jpg" as AnyObject])
            people.append(["id":3084 as AnyObject, "name":"Marlon Brando" as AnyObject, "profilePath":"/vklkhX4QlRKnEG8ylhWzoBdcuev.jpg" as AnyObject])
            people.append(["id":11288 as AnyObject, "name":"Robert Pattinson" as AnyObject, "profilePath":"/wNcm8RiMYlWvneAkqQepkqI6r7L.jpg" as AnyObject])
            people.append(["id":3223 as AnyObject, "name":"Robert Downey Jr." as AnyObject, "profilePath":"/dRLSoufWtc16F5fliK4ECIVs56p.jpg" as AnyObject])
            people.append(["id":4135 as AnyObject, "name":"Robert Redford" as AnyObject, "profilePath":"/b4mJIBWvEyzB8yyZWbmvfzwrdtp.jpg" as AnyObject])
            people.append(["id":380 as AnyObject, "name":"Robert De Niro" as AnyObject, "profilePath":"/lvTSwUcvJRLAJ2FB5qFaukel516.jpg" as AnyObject])
            people.append(["id":1158 as AnyObject, "name":"Al Pacino" as AnyObject, "profilePath":"/qLsGeBBKp16EJH1RR9pvAHav3ie.jpg" as AnyObject])
            people.append(["id":8784 as AnyObject, "name":"Daniel Craig" as AnyObject, "profilePath":"/rFuETZeyOAfIqBahOObF7Soq5Dh.jpg" as AnyObject])
            people.append(["id":10980 as AnyObject, "name":"Daniel Radcliffe" as AnyObject, "profilePath":"/kMSMa5tR43TLMR14ahU1neFVytz.jpg" as AnyObject])
            people.append(["id":11856 as AnyObject, "name":"Daniel Day-Lewis" as AnyObject, "profilePath":"/hknfCSSU6AMeKV9yn9NTtTzIEGc.jpg" as AnyObject])
            people.append(["id":4483 as AnyObject, "name":"Dustin Hoffman" as AnyObject, "profilePath":"/ffKPo8ATHVXME6cgA5BDyvy2df1.jpg" as AnyObject])
            people.append(["id":2524 as AnyObject, "name":"Tom Hardy" as AnyObject, "profilePath":"/4W8v3fX0viPRmwRtS0SfLJW8fkd.jpg" as AnyObject])
            people.append(["id":31 as AnyObject, "name":"Tom Hanks" as AnyObject, "profilePath":"/pQFoyx7rp09CJTAb932F2g8Nlho.jpg" as AnyObject])
            people.append(["id":4173 as AnyObject, "name":"Anthony Hopkins" as AnyObject, "profilePath":"/jdoBTIru71FbPuHGEgox5RVmIO0.jpg" as AnyObject])
            people.append(["id":5292 as AnyObject, "name":"Denzel Washington" as AnyObject, "profilePath":"/8vNlSf4EObsUn47LVS2EycfafNA.jpg" as AnyObject])
            people.append(["id":3895 as AnyObject, "name":"Michael Caine" as AnyObject, "profilePath":"/vvj0JMSFpOajXCE46Hy4dyqSP2U.jpg" as AnyObject])
            people.append(["id":2232 as AnyObject, "name":"Michael Keaton" as AnyObject, "profilePath":"/baeHNv3qrVsnApuKbZXiJOhqMnw.jpg" as AnyObject])
            people.append(["id":17051 as AnyObject, "name":"James Franco" as AnyObject, "profilePath":"/m9he3UnbmAAxkV1XH2EmzkNfkaS.jpg" as AnyObject])
            people.append(["id":5530 as AnyObject, "name":"James McAvoy" as AnyObject, "profilePath":"/aTenADGhPZUhuN6nfyz0Ayv6grb.jpg" as AnyObject])
            people.append(["id":2157 as AnyObject, "name":"Robin Williams" as AnyObject, "profilePath":"/5KebSMXT8uj2D0gkaMFJ8VEp53.jpg" as AnyObject])
            people.append(["id":32 as AnyObject, "name":"Robin Wright" as AnyObject, "profilePath":"/cke0NNZP4lHRtOethRy2XGSOp3E.jpg" as AnyObject])
            people.append(["id":192 as AnyObject, "name":"Morgan Freeman" as AnyObject, "profilePath":"/oGJQhOpT8S1M56tvSsbEBePV5O1.jpg" as AnyObject])
            people.append(["id":56903 as AnyObject, "name":"Tracy Morgan" as AnyObject, "profilePath":"/wXLPjDsy4SfqghEAJ8wybHh04nU.jpg" as AnyObject])
            people.append(["id":16897 as AnyObject, "name":"Sidney Poitier" as AnyObject, "profilePath":"/9LV17m3H7nGCJTXg7pzhK857g6V.jpg" as AnyObject])
            people.append(["id":190 as AnyObject, "name":"Clint Eastwood" as AnyObject, "profilePath":"/n8h4ZHteFFXfmzUW6OEaPWanDnm.jpg" as AnyObject])
            people.append(["id":1233 as AnyObject, "name":"Philip Seymour Hoffman" as AnyObject, "profilePath":"/vEDJukK8CjIsA0DjNFhoT88TvzS.jpg" as AnyObject])
            people.append(["id":6193 as AnyObject, "name":"Leonardo DiCaprio" as AnyObject, "profilePath":"/A85WIRIKVsD2DeUSc8wQ4fOKc4e.jpg" as AnyObject])
            people.append(["id":55638 as AnyObject, "name":"Kevin Hart" as AnyObject, "profilePath":"/9zxRAhWMxhVrgDnUysvTGLW7fcW.jpg" as AnyObject])
            people.append(["id":1979 as AnyObject, "name":"Kevin Spacey" as AnyObject, "profilePath":"/cdowETe1PgXLjo72hDb7R7tyavf.jpg" as AnyObject])
            people.append(["id":4690 as AnyObject, "name":"Christopher Walken" as AnyObject, "profilePath":"/ysO1GwRzLT9OVAB9Y2SKHxomqDr.jpg" as AnyObject])
            people.append(["id":1327 as AnyObject, "name":"Ian McKellen" as AnyObject, "profilePath":"/coWjgMEYJjk2OrNddlXCBm8EIr3.jpg" as AnyObject])
            people.append(["id":3810 as AnyObject, "name":"Javier Bardem" as AnyObject, "profilePath":"/hbr72lORyRjm8w9ujU5OA7YDzG.jpg" as AnyObject])
            people.append(["id":1810 as AnyObject, "name":"Heath Ledger" as AnyObject, "profilePath":"/zCemB5iknCmtSCcyNhkSA0LOgp1.jpg" as AnyObject])
            people.append(["id":5469 as AnyObject, "name":"Ralph Fiennes" as AnyObject, "profilePath":"/nt6wcXariJ2kQiMFocMUjvVP92A.jpg" as AnyObject])
            people.append(["id":85 as AnyObject, "name":"Johnny Depp" as AnyObject, "profilePath":"/ctaca3ALycPP0vPhRSYK5DTBEPF.jpg" as AnyObject])
            people.append(["id":134 as AnyObject, "name":"Jamie Foxx" as AnyObject, "profilePath":"/bcXscYxlsrHDenbo3BzK0f6pkaM.jpg" as AnyObject])
            people.append(["id":5472 as AnyObject, "name":"Colin Firth" as AnyObject, "profilePath":"/kbs5HzE2KjzbKiGYQw2aXFpdvaX.jpg" as AnyObject])
            people.append(["id":72466 as AnyObject, "name":"Colin Farrell" as AnyObject, "profilePath":"/pgMM9r5ll99RXSmz1J4sj8zaddW.jpg" as AnyObject])
            people.append(["id":10297 as AnyObject, "name":"Matthew McConaughey" as AnyObject, "profilePath":"/jdRmHrG0TWXGhs4tO6TJNSoL25T.jpg" as AnyObject])
            people.append(["id":14408 as AnyObject, "name":"Matthew Perry" as AnyObject, "profilePath":"/oSKEEDXDNnwWdQ68qfDVD6Q7Pxp.jpg" as AnyObject])
            people.append(["id":36424 as AnyObject, "name":"LL Cool J" as AnyObject, "profilePath":"/dyDxzZo3buNTntxWehyW66aS5Mo.jpg" as AnyObject])
            people.append(["id":3894 as AnyObject, "name":"Christian Bale" as AnyObject, "profilePath":"/17T0HN2OBiyDLXtsyzS5zznd6YP.jpg" as AnyObject])
            people.append(["id":64 as AnyObject, "name":"Gary Oldman" as AnyObject, "profilePath":"/tofLS5A6lBXNjeROGvgpfe2JwaT.jpg" as AnyObject])
            people.append(["id":819 as AnyObject, "name":"Edward Norton" as AnyObject, "profilePath":"/eIkFHNlfretLS1spAcIoihKUS62.jpg" as AnyObject])
            people.append(["id":287 as AnyObject, "name":"Brad Pitt" as AnyObject, "profilePath":"/ejYIW1enUcGJ9GS3Bs34mtONwWS.jpg" as AnyObject])
            people.append(["id":1892 as AnyObject, "name":"Matt Damon" as AnyObject, "profilePath":"/elSlNgV8xVifsbHpFsqrPGxJToZ.jpg" as AnyObject])
            people.append(["id":3896 as AnyObject, "name":"Liam Neeson" as AnyObject, "profilePath":"/r1DRqmZhLrzhTTwg9PtuMMQTvuB.jpg" as AnyObject])
            people.append(["id":2461 as AnyObject, "name":"Mel Gibson" as AnyObject, "profilePath":"/6VGgL0bBvPIJ9vDOyyGf5nK2zL4.jpg" as AnyObject])
            people.append(["id":3 as AnyObject, "name":"Harrison Ford" as AnyObject, "profilePath":"/aVKNqtkzZCymupOrvwxozamRyVc.jpg" as AnyObject])
            people.append(["id":2 as AnyObject, "name":"Mark Hamill" as AnyObject, "profilePath":"/ws544EgE5POxGJqq9LUfhnDrHtV.jpg" as AnyObject])
            people.append(["id":2231 as AnyObject, "name":"Samuel L. Jackson" as AnyObject, "profilePath":"/dlW6prW9HwYDsIRXNoFYtyHpSny.jpg" as AnyObject])
            people.append(["id":206 as AnyObject, "name":"Jim Carrey" as AnyObject, "profilePath":"/5tVf0ow8MX4OwjmVoSa5v7qUDka.jpg" as AnyObject])
            people.append(["id":1896 as AnyObject, "name":"Don Cheadle" as AnyObject, "profilePath":"/4PvWfLvABO5n1ZfzK76vol9Bqae.jpg" as AnyObject])
            people.append(["id":2888 as AnyObject, "name":"Will Smith" as AnyObject, "profilePath":"/2iYXDlCvLyVO49louRyDDXagZ0G.jpg" as AnyObject])
            people.append(["id":2047 as AnyObject, "name":"Danny Glover" as AnyObject, "profilePath":"/jSNTEnm0Sxm8FRtoBfJmhmQyozH.jpg" as AnyObject])
            people.append(["id":119589 as AnyObject, "name":"Donald Glover" as AnyObject, "profilePath":"/dHQ9gpLg2fGUUHNy5zVQ4zNgcYC.jpg" as AnyObject])
            people.append(["id":9778 as AnyObject, "name":"Ice Cube" as AnyObject, "profilePath":"/dzdn1tyWkC4EjlBVKvpAhg5osYA.jpg" as AnyObject])
            people.append(["id":15152 as AnyObject, "name":"James Earl Jones" as AnyObject, "profilePath":"/2ZuBf3ip2RXhkiQqGUjbUzAf4Nx.jpg" as AnyObject])
            people.append(["id":2178 as AnyObject, "name":"Forest Whitaker" as AnyObject, "profilePath":"/4pMQkelS5lK661m9Kz3oIxLYiyS.jpg" as AnyObject])
            people.append(["id":776 as AnyObject, "name":"Eddie Murphy" as AnyObject, "profilePath":"/bsi706zdqs0KAPdOsqz6AkLNcFF.jpg" as AnyObject])
            people.append(["id":2975 as AnyObject, "name":"Laurence Fishburne" as AnyObject, "profilePath":"/8suOhUmPbfKqDQ17jQ1Gy0mI3P4.jpg" as AnyObject])
            people.append(["id":10814 as AnyObject, "name":"Wesley Snipes" as AnyObject, "profilePath":"/hQ6EBa6vgu7HoZpzms8Y10VL5Iw.jpg" as AnyObject])
            people.append(["id":2395 as AnyObject, "name":"Whoopi Goldberg" as AnyObject, "profilePath":"/n3lF8w4X4rDa4J2LMDIxMEcuUeH.jpg" as AnyObject])
            people.append(["id":13309 as AnyObject, "name":"Oprah Winfrey" as AnyObject, "profilePath":"/jlsGiD71V3sZYrGG5aNgcQsElMn.jpg" as AnyObject])
            people.append(["id":9777 as AnyObject, "name":"Cuba Gooding Jr." as AnyObject, "profilePath":"/yu8Q3ImFu3RJne585jjgeQO2Boo.jpg" as AnyObject])
            people.append(["id":938 as AnyObject, "name":"Djimon Hounsou" as AnyObject, "profilePath":"/y22Pb0XAVqC0l7ukzKKtXzPEuHk.jpg" as AnyObject])
            people.append(["id":19492 as AnyObject, "name":"Viola Davis" as AnyObject, "profilePath":"/fZDYv4yIUOvnELoKzXpIWx8YNlq.jpg" as AnyObject])
            people.append(["id":9780 as AnyObject, "name":"Angela Bassett" as AnyObject, "profilePath":"/tHkgSzhEuJKp5hqp0DZLad8HNZ9.jpg" as AnyObject])
            people.append(["id":18918 as AnyObject, "name":"Dwayne Johnson" as AnyObject, "profilePath":"/akweMz59qsSoPUJYe7QpjAc2rQp.jpg" as AnyObject])
            people.append(["id":72129 as AnyObject, "name":"Jennifer Lawrence" as AnyObject, "profilePath":"/kv8kph9zKuXBmkxSL7XnLGa0yjH.jpg" as AnyObject])
            people.append(["id":6161 as AnyObject, "name":"Jennifer Connelly" as AnyObject, "profilePath":"/xTpRRy9hwk4E2uypuQ30iqt2W2W.jpg" as AnyObject])
            people.append(["id":1204 as AnyObject, "name":"Julia Roberts" as AnyObject, "profilePath":"/yzaIyUEKHSnEYDwltXs8gpF4SVC.jpg" as AnyObject])
            people.append(["id":12041 as AnyObject, "name":"Julia Stiles" as AnyObject, "profilePath":"/s7OC22crrqBo8qjI5TGk4hRKd44.jpg" as AnyObject])
            people.append(["id":6885 as AnyObject, "name":"Charlize Theron" as AnyObject, "profilePath":"/4PLynhfkAa0WFUXd3frSpKtaG1V.jpg" as AnyObject])
            people.append(["id":9273 as AnyObject, "name":"Amy Adams" as AnyObject, "profilePath":"/5lYjMXl7xhG4gUu1XYqh6Dsni2K.jpg" as AnyObject])
            people.append(["id":5064 as AnyObject, "name":"Meryl Streep" as AnyObject, "profilePath":"/oTJj6bLpbmseLww03MOn0eDqYuh.jpg" as AnyObject])
            people.append(["id":1245 as AnyObject, "name":"Scarlett Johansson" as AnyObject, "profilePath":"/8EueDe6rPF0jQU4LSpsH2Rmrqac.jpg" as AnyObject])
            people.append(["id":524 as AnyObject, "name":"Natalie Portman" as AnyObject, "profilePath":"/jJcRWku3e9OHrmRqytn6WcBjhvh.jpg" as AnyObject])
            people.append(["id":2227 as AnyObject, "name":"Nicole Kidman" as AnyObject, "profilePath":"/gIadzRlFnMb1U58M7LKIMq2YUR4.jpg" as AnyObject])
            people.append(["id":112 as AnyObject, "name":"Cate Blanchett" as AnyObject, "profilePath":"/5HikVWKfkkUa8aLdCMHtREBECIn.jpg" as AnyObject])
            people.append(["id":10990 as AnyObject, "name":"Emma Watson" as AnyObject, "profilePath":"/s77hUycSJ4x8RJWHDC9WPgotgxE.jpg" as AnyObject])
            people.append(["id":54693 as AnyObject, "name":"Emma Stone" as AnyObject, "profilePath":"/lhjnYohEn2HflqCEnWuuilcNUsj.jpg" as AnyObject])
            people.append(["id":204 as AnyObject, "name":"Kate Winslet" as AnyObject, "profilePath":"/w8wjPbS24vPErNeYhAvtbyAUBMd.jpg" as AnyObject])
            people.append(["id":18277 as AnyObject, "name":"Sandra Bullock" as AnyObject, "profilePath":"/bsAy8f8UZKairXQzRukU5FP4XAQ.jpg" as AnyObject])
            people.append(["id":1038 as AnyObject, "name":"Jodie Foster" as AnyObject, "profilePath":"/eAIE6bnOQ8rm0f933gyeAQdIwrP.jpg" as AnyObject])
            people.append(["id":7056 as AnyObject, "name":"Emma Thompson" as AnyObject, "profilePath":"/cWTBHN8kLf6yapxiaQD9C6N1uMw.jpg" as AnyObject])
            people.append(["id":1231 as AnyObject, "name":"Julianne Moore" as AnyObject, "profilePath":"/nE6m77H9XoqlDCabNvUA9CykoHK.jpg" as AnyObject])
            people.append(["id":18973 as AnyObject, "name":"Mila Kunis" as AnyObject, "profilePath":"/tc2JwjqC04FckKLuVdRVV2ZdtHn.jpg" as AnyObject])
            people.append(["id":116 as AnyObject, "name":"Keira Knightley" as AnyObject, "profilePath":"/rv6quYbTgFTmBAoePwy5xuurW3g.jpg" as AnyObject])
            DataManager.shared.setStartPeopleData(people)
            
            // Set defaults
            DataManager.shared.setStartPersonData(people[0])
            DataManager.shared.setStartPersonIsRandom(true)
            DataManager.shared.setEndPersonData(dataFromPerson(Person(id: kKevinBaconID, name: kKevinBaconName, character: UNKNOWN, profilePath: kKevinBaconProfilePath, knownFor:nil)))
            DataManager.shared.setEndPersonIsRandom(false)
        }
    }
    
    // MARK: Configuration Info
    class func getConfigurationInfo(_ completion:@escaping (Bool, String?) -> Void) {
        //https://api.themoviedb.org/3/configuration?api_key=dcdec012333250c1bf0eb35a8b832559
        WebService.shared.getDataForResource("configuration", params: []) { (success, jsonObject, errorMessage) in
            DispatchQueue.main.async {
                if !success {
                    completion(false, errorMessage)
                    return
                }
                // Defaults
                var _baseUrl = "http://image.tmdb.org/t/p/"
                var _posterMedium = "w185"
                
                if let json = jsonObject as? [String:Any] {
                    if let images = json["images"] as? [String:Any] {
                        if let base_url = images["base_url"] as? String {
                            _baseUrl = base_url
                        }
                        if let posterSizes = images["poster_sizes"] as? [String] {
                            if posterSizes.count > 5 {
                                _posterMedium = posterSizes[2]
                            }
                        }
                    }
                }
                DataManager.shared.setImageBaseUrl(_baseUrl)
                DataManager.shared.setPosterSizeMedium(_posterMedium)
                DataManager.shared.setConfigurationIsSet(true)
                completion(true, nil)
            }
        }
    }
    
    // MARK: Start Person
    class func getRandomPerson(otherPerson:Person?) -> Person? {
        let personData:[[String:AnyObject]] = DataManager.shared.getStartPeopleData()
        if personData.count > 0 {
            func random() -> Person {
                let randomIndex:Int = Int(arc4random_uniform(UInt32(personData.count)))
                let randomPersonData = personData[randomIndex]
                return self.personFromData(randomPersonData)
            }
            // If otherPerson is not nil then make sure the random person is not the same person as otherPerson
            if let otherPerson = otherPerson {
                var randomPerson:Person!
                repeat {
                    randomPerson = random()
                } while randomPerson.id == otherPerson.id
                return randomPerson
            } else {
                return random()
            }
        }
        return nil
    }
    
    fileprivate class func personFromData(_ data:[String:AnyObject]) -> Person {
        if let id = data["id"] as? Int, let name = data["name"] as? String, let profilePath = data["profilePath"] as? String {
            let path:String? = profilePath == "" ? nil : profilePath
            return Person(id: id, name: name, character: UNKNOWN, profilePath: path, knownFor:nil)
        }
        return Person(id: -1, name: UNKNOWN, character: UNKNOWN, profilePath: nil, knownFor:nil)
    }
    
    fileprivate class func dataFromPerson(_ person:Person) -> [String:AnyObject] {
        let profilePath = person.profilePath == nil ? "" : person.profilePath!
        return ["id":person.id as AnyObject, "name":person.name as AnyObject, "profilePath":profilePath as AnyObject]
    }
    
    // This is a complicated function that is used to add people to the list of available start people
    // The list starts out with 100 people hard coded into the app
    // See original list of people above in appDidLaunch
    // Every time a movie cast is retrieved from the API it calls this function and passes in the list of people in the cast
    // Here is the list of steps this function takes to add people to the available start people
    // 0. Check to make sure the number of people in the cast list is at least 5
    // 1. Check the list of people passed in for Kevin Bacon
    // 2. Return from the function if KB is in the list. We don't want anyone (known to be) that close to KB
    // 3. If KB is NOT in the list then select the first person from the list. Generally the top person in the list is one of the most popular cast members
    // 4. Make sure that person is not already in the list of start people
    // 5. If there are less than 250 people in the start people array then add the person to the list
    // 6. if there are 250 or more in the start people array then insert the person in the list (overwrite another person)
    class func addPersonToStartPeopleFromList(_ people:[Person]) {
        // Return if the cast is too small. Generally a tiny cast means a less popular movie.
        if people.count < 5 {
            return
        }
        // Make sure Kevin Bacon is not in the list of people
        for person in people {
            if person.id == kKevinBaconID {
                return
            }
        }
        // Select the first person from the list of people
        let newPerson:Person = people[0]
        
        // If the random person does not have a photo then they are not famous enough :)
        if newPerson.profilePath == nil {
            return
        }
        
        // Get the list of start people
        var existingPeople = DataManager.shared.getStartPeopleData()

        // Check to make sure the list of start people does not contain the random person
        for existingPerson in existingPeople {
            if let id = existingPerson["id"] as? Int {
                if id == newPerson.id {
                    return
                }
            }
        }
        
        // Add the random person to the start people
        // Keep the list of start people at or below 250
        if existingPeople.count < 250 {
            existingPeople.append(BL.dataFromPerson(newPerson))
        } else {
            // Insert the new person into the last 150 people. After the original 100.
            let randomIndex = Int(arc4random_uniform(UInt32(150)))+100
            if (randomIndex < 250 && randomIndex > 99) {
                existingPeople[randomIndex] = BL.dataFromPerson(newPerson)
            }
        }
        DataManager.shared.setStartPeopleData(existingPeople)
    }
    
    // MARK: First and Last
    class func getFirstPerson() -> Person? {
        return DataManager.shared.firstPerson
    }
    class func setFirstPerson(_ person:Person) {
        DataManager.shared.firstPerson = person
    }
    class func getLastPerson() -> Person? {
        return DataManager.shared.lastPerson
    }
    class func setLastPerson(_ person:Person) {
        DataManager.shared.lastPerson = person
    }
    
    // MARK: Start Person
    class func getStartPerson() -> Person? {
        if let data = DataManager.shared.getStartPersonData() {
            return personFromData(data)
        } else {
            return nil
        }
    }
    class func setStartPerson(_ person:Person) {
        DataManager.shared.setStartPersonIsRandom(false)
        DataManager.shared.setStartPersonData(dataFromPerson(person))
    }
    class func getStartPersonRandom() -> Bool {
        return DataManager.shared.getStartPersonIsRandom()
    }
    class func setStartPersonRandom(_ isRandom:Bool) {
        DataManager.shared.setStartPersonIsRandom(isRandom)
    }
    
    // MARK: End Person
    class func getEndPerson() -> Person? {
        if let data = DataManager.shared.getEndPersonData() {
            return personFromData(data)
        } else {
            return nil
        }
    }
    class func setEndPerson(_ person:Person) {
        DataManager.shared.setEndPersonIsRandom(false)
        DataManager.shared.setEndPersonData(dataFromPerson(person))
    }
    class func getEndPersonRandom() -> Bool {
        return DataManager.shared.getEndPersonIsRandom()
    }
    class func setEndPersonRandom(_ isRandom:Bool) {
        DataManager.shared.setEndPersonIsRandom(isRandom)
    }
    
    // MARK: Selected Person
    class func getSelectedPerson() -> Person? {
        return DataManager.shared.selectedPerson
    }
    
    class func setSelectedPerson(_ person:Person) {
        DataManager.shared.selectedPerson = person
        DataManager.shared.selectedMedia = nil
    }
    
    // MARK: Selected Media
    class func getSelectedMedia() -> Media? {
        return DataManager.shared.selectedMedia
    }
    
    class func setSelectedMedia(_ media:Media) {
        DataManager.shared.selectedMedia = media
        DataManager.shared.selectedPerson = nil
    }
    
    // MARK: Image URL
    class func imageUrl(_ path:String) -> String {
        return DataManager.shared.getImageBaseUrl() + DataManager.shared.getPosterSizeMedium() + path
    }
    
    class func searchForPeople(_ searchString:String, completion:@escaping (Bool, [Person]?, String?) -> Void) {
        // api.themoviedb.org/3/search/person?api_key=dcdec012333250c1bf0eb35a8b832559&language=en-US&query=kevin%20bacon&page=1&include_adult=false
        let params = ["language=en-US", "query=" + searchString, "page=1", "include_adult=false"]
        WebService.shared.getDataForResource("search/person", params:params) { (success:Bool, json:Any?, errorMessage:String?) in
            DispatchQueue.main.async {
                if !success {
                    completion(false, nil, errorMessage)
                    return
                }
                guard let json = json as? [String:Any] else {
                    completion(false, nil, errorMessage)
                    return
                }
                if let personJsonArray = json["results"] as? [[String:Any]] {
                    var personArray:[Person] = []
                    for personJson:[String:Any] in personJsonArray {
                        do {
                            let person = try Person(json: personJson)
                            personArray.append(person)
                        } catch {
                            continue
                        }
                    }
                    completion(true, personArray, nil)
                } else {
                    completion(false, nil, errorMessage)
                }
            }
        }
    }

    class func getMovieCredits(_ completion:@escaping (Bool, [Person]?, String?) -> Void) {
        guard let id = BL.getSelectedMedia()?.id else {
            completion(false, nil, nil)
            return
        }
        WebService.shared.getDataForResource("movie/\(id)/credits", params:[]) { (success:Bool, json:Any?, errorMessage:String?) in
            DispatchQueue.main.async {
                if !success {
                    completion(false, nil, errorMessage)
                    return
                }
                guard let json = json as? [String:Any] else {
                    completion(false, nil, errorMessage)
                    return
                }
                if let personJsonArray = json["cast"] as? [[String:Any]] {
                    var personArray:[Person] = []
                    for personJson:[String:Any] in personJsonArray {
                        do {
                            let person = try Person(json: personJson)
                            personArray.append(person)
                        } catch {
                            continue
                        }
                    }
                    BL.addPersonToStartPeopleFromList(personArray)
                    completion(true, personArray, nil)
                } else {
                    completion(false, nil, errorMessage)
                }
            }
        }
    }
    
    class func getPersonCredits(_ completion:@escaping (Bool, [Media]?, String?) -> Void) {
        guard let id = BL.getSelectedPerson()?.id else {
            completion(false, nil, nil)
            return
        }
        WebService.shared.getDataForResource("person/\(id)/movie_credits", params:[]) { (success:Bool, json:Any?, errorMessage:String?) in
            DispatchQueue.main.async {
                if !success {
                    completion(false, nil, errorMessage)
                    return
                }
                guard let json = json as? [String:Any] else {
                    completion(false, nil, errorMessage)
                    return
                }
                if let mediaJsonArray = json["cast"] as? [[String:Any]] {
                    var mediaArray:[Media] = []
                    for mediaJson:[String:Any] in mediaJsonArray {
                        do {
                            let movie = try Movie(json: mediaJson)
                            mediaArray.append(movie)
                        } catch {
                            continue
                        }
                    }
                    let returnArray = mediaArray.filter({ (media:Media) -> Bool in
                        if let date = media.date {
                            if date < Date() {
                                return true
                            }
                        }
                        return false
                    })
                    completion(true, returnArray, nil)
                } else {
                    completion(false, nil, errorMessage)
                }
            }
        }
    }
    
    class func addProfileImageWithPath(_ path:String?, imageView:UIImageView, activityIndicator:UIActivityIndicatorView?){
        if let path = path {
            if let escapedString = BL.imageUrl(path).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
                let fullUrl:URL = URL(string: escapedString)!
                let newActivityIndicator = UIActivityIndicatorView(frame: imageView.bounds)
                if let activityIndicator = activityIndicator {
                    activityIndicator.startAnimating()
                } else {
                    newActivityIndicator.activityIndicatorViewStyle = .white
                    newActivityIndicator.startAnimating()
                    imageView.addSubview(newActivityIndicator)
                }
                let placeholder:UIImage = UIImage(named: "Placeholder")!
                imageView.sd_setImage(with: fullUrl, placeholderImage: placeholder, options: [], completed: { (image:UIImage?, error:Error?, cacheType:Any, url:URL?) in
                    DispatchQueue.main.async {
                        if let activityIndicator = activityIndicator {
                            activityIndicator.removeFromSuperview()
                        } else {
                            newActivityIndicator.removeFromSuperview()
                        }
                        if image == nil {
                            imageView.image = UIImage(named: "NoImage")
                            
                            
                            // TODO BAD INTERNET CONNECTION CRASHES APP HERE
                            
                            
                        }
                    }
                })
            }
        } else {
            imageView.image = UIImage(named: "NoImage")
        }
    }
    
    class func addPosterImageWithPath(_ path:String?, imageView:UIImageView){
        if let path = path {
            if let escapedString = BL.imageUrl(path).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
                let fullUrl:URL = URL(string: escapedString)!
                let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: imageView.frame.width, height: imageView.frame.height))
                activityIndicator.startAnimating()
                imageView.addSubview(activityIndicator)
                let placeholder:UIImage = UIImage(named: "Placeholder")!
                imageView.sd_setImage(with: fullUrl, placeholderImage: placeholder, options: [], completed: { (image:UIImage?, error:Error?, cacheType:Any, url:URL?) in
                    DispatchQueue.main.async {
                        activityIndicator.removeFromSuperview()
                        if image == nil {
                            imageView.image = UIImage(named: "NoImage")
                        }
                    }
                })
            }
        } else {
            imageView.image = UIImage(named: "NoImage")
        }
    }
    
    // In-App Purchase
    class func ableToSelectPeople() -> Bool {
        return DataManager.shared.getAbleToSelectPeople()
    }
    class func successfullyMadePurchase() {
        DataManager.shared.setAbleToSelectPeople(true)
    }
    
    // Data for asking user to rate app
    class func getNumberOfWins() -> Int {
        return DataManager.shared.getNumberOfWins()
    }
    class func incrementNumberOfWins() {
        let numberOfWins = DataManager.shared.getNumberOfWins()
        DataManager.shared.setNumberOfWins(numberOfWins + 1)
    }
    
    class func getHasBeenAskedToRate() -> Bool {
        return DataManager.shared.getHasBeenAskedToRate()
    }
    class func askedUserToRate() {
        DataManager.shared.setHasBeenAskedToRate(true)
    }
}
