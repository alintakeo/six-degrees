//
//  MessageBox.swift
//  SixDegrees
//
//  Created by Alin Nomura on 8/17/17.
//  Copyright © 2017 Nomura. All rights reserved.
//

import UIKit

class MessageBox: NSObject {
    var title = ""
    var message = ""
    var button1Title:String?
    var button2Title:String?
    var button1DoesDismiss = false
    var button2DoesDismiss = false
    var titleSelector:Selector?
    var button1Selector:Selector?
    var button2Selector:Selector?
    var hasDismissButton = false
    
    private var bounds:CGRect!
    private var shadow:UIView!
    private var panel:UIView!
    private var dismissButton:UIButton?
    
    func show(controller:UIViewController) {
        self.bounds = controller.view.bounds
        self.shadow = UIView(frame: self.bounds)
        self.shadow.backgroundColor = UIColor.clear
        controller.view.addSubview(self.shadow)
        
        let panelWidth:CGFloat = bounds.width - 40
        let headerHeight:CGFloat = 40
        let buttonHeight:CGFloat = 40
        let spacing:CGFloat = 8
        
        self.panel = UIView(frame: CGRect(x: -panelWidth - 20, y: 0, width: panelWidth, height: 0)) // Set verticle measurements at the end
        self.panel.backgroundColor = UI.DarkColor()
        self.panel.layer.borderColor = UI.MainBlue().cgColor
        self.panel.layer.borderWidth = 1
        self.panel.layer.cornerRadius = buttonHeight/2
        self.panel.clipsToBounds = true
        self.shadow.addSubview(self.panel)
        
        var y:CGFloat = 0
        
        if let titleSelector = titleSelector {
            let header = UIButton(frame: CGRect(x: 0, y: y, width: panelWidth, height: headerHeight))
            header.setTitle(self.title, for: UIControlState.normal)
            header.backgroundColor = UI.MainBlue()
            header.titleLabel?.textColor = UIColor.white
            header.titleLabel?.font = UIFont(name: kFontBoldName, size: 18)
            header.addTarget(controller, action: titleSelector, for: UIControlEvents.touchUpInside)
            self.panel.addSubview(header)
        } else {
            let header = UILabel(frame: CGRect(x: 0, y: y, width: panelWidth, height: headerHeight))
            header.text = self.title
            header.textAlignment = .center
            header.layer.borderWidth = 1
            header.layer.borderColor = UI.MainBlue().cgColor
            header.textColor = UIColor.white
            header.font = UIFont(name: kFontBoldName, size: 18)
            self.panel.addSubview(header)
        }
        
        y += headerHeight
        y += spacing * 3
        
        let body = UITextView(frame: CGRect(x: spacing, y: y, width: panelWidth - (spacing * 2), height: 1000))
        body.isEditable = false
        body.isSelectable = false
        body.text = self.message
        body.textColor = UIColor.white
        body.font = UIFont(name: kFontName, size: 14)
        body.textAlignment = .center
        body.backgroundColor = UIColor.clear
        body.sizeToFit()
        let maxBodyHeight:CGFloat = 420
        body.isScrollEnabled = body.frame.height > maxBodyHeight ? true : false
        let bodyHeight = body.frame.height > maxBodyHeight ? maxBodyHeight : body.frame.height
        //After call to sizeToFit the width will need to be reset to center properly
        body.frame = CGRect(x: spacing, y: y, width: panelWidth - (spacing * 2), height: bodyHeight)
        self.panel.addSubview(body)
        
        y += body.frame.height
        y += spacing * 3
        
        if self.button1Title != nil {
            var twoButtons = false
            if self.button2Title != nil {
                twoButtons = true
            }
            let buttonWidth:CGFloat = twoButtons ? (panelWidth - (3 * spacing)) / 2 : 250
            let x1:CGFloat = twoButtons ? spacing : (panelWidth - buttonWidth) / 2
            let button1 = UIButton(frame: CGRect(x: x1, y: y, width: buttonWidth, height: buttonHeight))
            button1.setTitle(self.button1Title, for: UIControlState.normal)
            button1.titleLabel?.font = UIFont(name: kFontName, size: 14)
            button1.layer.cornerRadius = buttonHeight / 2
            button1.backgroundColor = UI.MainBlue()
            button1.titleLabel?.textColor = UIColor.white
            if self.button1DoesDismiss {
                button1.addTarget(self, action: #selector(MessageBox.dismiss), for: UIControlEvents.touchUpInside)
            }
            if let button1Selector = self.button1Selector {
                button1.addTarget(controller, action: button1Selector, for: UIControlEvents.touchUpInside)
            }
            self.panel.addSubview(button1)
            
            if twoButtons {
                let x2:CGFloat = (panelWidth + spacing) / 2
                let button2 = UIButton(frame: CGRect(x: x2, y: y, width: buttonWidth, height: buttonHeight))
                button2.setTitle(self.button2Title, for: UIControlState.normal)
                button2.titleLabel?.font = UIFont(name: kFontName, size: 14)
                button2.layer.cornerRadius = buttonHeight / 2
                button2.backgroundColor = UI.MainBlue()
                button2.titleLabel?.textColor = UIColor.white
                if self.button2DoesDismiss {
                    button2.addTarget(self, action: #selector(MessageBox.dismiss), for: UIControlEvents.touchUpInside)
                }
                if let button2Selector = self.button2Selector {
                    button2.addTarget(controller, action: button2Selector, for: UIControlEvents.touchUpInside)
                }
                self.panel.addSubview(button2)
            }
            y += buttonHeight
            y += spacing
        }
        
        self.panel.frame = CGRect(x: self.panel.frame.origin.x, y: (self.bounds.height - y) / 2, width: self.panel.frame.width, height: y)
        
        if self.hasDismissButton {
            let dismissWidth:CGFloat = 40
            let dismissX:CGFloat = (self.panel.frame.origin.x + self.panel.frame.width) - (dismissWidth / 2)
            let dismissY:CGFloat = self.panel.frame.origin.y - (dismissWidth / 2)
            self.dismissButton = UIButton(frame: CGRect(x: dismissX, y: dismissY, width: dismissWidth, height: dismissWidth))
            self.dismissButton!.setTitle("X", for: UIControlState.normal)
            self.dismissButton!.titleLabel?.font = UIFont(name: kDismissXFontName, size: 22)
            self.dismissButton!.layer.cornerRadius = dismissWidth / 2
            self.dismissButton!.backgroundColor = UI.DarkColor()
            self.dismissButton!.titleLabel?.textColor = UI.MainBlue()
            self.dismissButton!.layer.borderWidth = 1
            self.dismissButton!.layer.borderColor = UI.MainBlue().cgColor
            self.dismissButton!.addTarget(self, action: #selector(MessageBox.dismiss), for: UIControlEvents.touchUpInside)
            self.shadow.addSubview(self.dismissButton!)
        }
        
        let newPanelLocation = CGPoint(x: (self.bounds.width - self.panel.frame.width) / 2, y: (self.bounds.height - self.panel.frame.height) / 2)
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.shadow.backgroundColor = UIColor.black.withAlphaComponent(0.8)
            self.panel.frame = CGRect(x: newPanelLocation.x, y: newPanelLocation.y, width: self.panel.frame.width, height: self.panel.frame.height)
            if let dismissButton = self.dismissButton {
                dismissButton.frame = CGRect(x: (newPanelLocation.x + panelWidth) - (dismissButton.frame.width / 2), y: dismissButton.frame.origin.y, width: dismissButton.frame.width, height: dismissButton.frame.height)
            }
        }, completion: nil)
    }
    
    func dismiss() {
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.shadow.backgroundColor = UIColor.clear
            self.panel.frame = CGRect(x: -self.panel.frame.width - 20, y: self.panel.frame.origin.y, width: self.panel.frame.width, height: self.panel.frame.height)
            if let dismissButton = self.dismissButton {
                dismissButton.frame = CGRect(x: -dismissButton.frame.width, y: dismissButton.frame.origin.y, width: dismissButton.frame.width, height: dismissButton.frame.height)
            }
        }, completion: { (finished:Bool) in
            self.shadow.removeFromSuperview()
        })
    }
}
