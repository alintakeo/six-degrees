//
//  Media.swift
//  SixDegrees
//
//  Created by Alin Nomura on 1/16/16.
//  Copyright © 2016 Nomura. All rights reserved.
//

import Foundation

enum ModelError: Error {
    case nilID
    case adultContent
}

class Person {
    let id:Int
    let name:String
    let character:String
    let profilePath:String?
    let knownFor:String?
    
    init(id:Int, name:String, character:String, profilePath:String?, knownFor:String?) {
        self.id = id
        self.name = name
        self.character = character
        self.profilePath = profilePath
        self.knownFor = knownFor
    }

    init(json:[String:Any]) throws {
        guard let id = json["id"] as? Int else {
            throw ModelError.nilID
        }
        self.id = id
        
        var _name = UNKNOWN
        var _character = UNKNOWN
        var _profilePath:String? = nil
        var _knownFor:String? = nil
        
        if let name = json["name"] as? String, name != "" {
            _name = name
        }
        if let character = json["character"] as? String, character != "" {
            _character = character
        }
        if let profilePath = json["profile_path"] as? String {
            _profilePath = profilePath
        }
        if let knownFors = json["known_for"] as? [[String:Any]] {
            for media in knownFors {
                if let type = media["media_type"] as? String, let title = media["title"] as? String {
                    if type == "movie" {
                        _knownFor = title
                        break
                    }
                }
            }
        }
        self.name = _name
        self.character = _character
        self.profilePath = _profilePath
        self.knownFor = _knownFor
    }
}

// The Media class represents a movie or tv connection between two people
// TV has been nixed
class Media {
    let id:Int
    let type:String
    let title:String
    let date:Date?
    let posterPath:String?
    
    init(id:Int, type:String, title:String?, date:String?, posterPath:String?) {
        self.id = id
        self.type = type
        self.title = title == nil ? UNKNOWN : title!
        if let date = date {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            if let formattedDate = dateFormatter.date(from: date) {
                self.date = formattedDate
            } else {
                self.date = nil
            }
        } else {
            self.date = nil
        }
        self.posterPath = posterPath
    }
}

class Movie: Media {
    init(json:[String:Any]) throws {
        guard let id = json["id"] as? Int else {
            throw ModelError.nilID
        }
        if let adultContent = json["adult"] as? Bool, adultContent {
            throw ModelError.adultContent
        }
        let type = "Movie"
        let title = json["title"] as? String
        let date = json["release_date"] as? String
        let posterPath = json["poster_path"] as? String
        super.init(id: id, type: type, title: title, date: date, posterPath: posterPath)
    }
}
