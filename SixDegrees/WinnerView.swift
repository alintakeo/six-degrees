//
//  WinnerView.swift
//  SixDegrees
//
//  Created by Alin Nomura on 1/30/16.
//  Copyright © 2016 Nomura. All rights reserved.
//

import UIKit

class WinnerView: UIView {
    
    var delegate:PlayViewController?
    
    init(frame:CGRect, controller:PlayViewController, firstName:String, lastName:String, steps:Int) {
        super.init(frame: frame)
        self.delegate = controller
        self.build(firstName: firstName, lastName: lastName, steps: steps)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func build(firstName:String, lastName:String, steps:Int) {
        let background = UIView(frame: self.bounds)
        background.backgroundColor = UI.DarkColor()
        background.alpha = 0
        self.addSubview(background)
        UIView.animate(withDuration: 0.5) {
            background.alpha = 1
        }
        
        enum LabelType {
            case header, person, small
        }
        var delay:TimeInterval = 0
        func addLine(_ text:String, top:CGFloat, type:LabelType) -> CGFloat {
            var font:UIFont!
            var height:CGFloat!
            var color = UIColor.white
            switch type {
            case .header:
                font = UIFont(name: kFontName, size: 30)
                height = 50
            case .person:
                font = UIFont(name: kFontName, size: 16)
                height = 40
                color = UI.MainBlue()
            case .small:
                font = UIFont(name: kFontBoldName, size: 14)
                height = 21
            }
            let x:CGFloat = type == .person ? -1 : 0 // -1 to hide left border
            let w:CGFloat = type == .person ? self.frame.width * 3: self.frame.width;
            let label:UILabel = UILabel(frame: CGRect(x: x, y: top, width: w, height: height))
            label.font = font
            label.text = text
            label.textColor = color
            label.textAlignment = NSTextAlignment.center

            self.addSubview(label)
            
            if type == .person {
                delay += 0.2
                label.layer.borderWidth = 1
                label.backgroundColor = UIColor.black.withAlphaComponent(0.3)
                label.layer.borderColor = UI.MainBlue().cgColor
                label.alpha = 0
                
                UIView.animate(withDuration: 0.3, delay: delay, options: .curveEaseOut, animations: {
                    label.alpha = 1
                    label.frame = CGRect(x: -self.frame.width, y: label.frame.origin.y, width: label.frame.width, height: label.frame.height)
                }, completion: nil)
            }
            return top + height
        }
        let buttonHeight:CGFloat = 40
        let totalHeight:CGFloat = 239 + 140 // Height of all text lines and the steps circle. Manually calculated.
        let topText = UIScreen.main.nativeBounds.height < 1334 ? "" : "CONGRATULATIONS!" // No text here for shorter screens
        var y = addLine(topText, top: ((self.frame.height - (buttonHeight + 8)) - totalHeight) / 2, type: .header)
        y = addLine("YOU LINKED", top: y + 8, type: .small)
        y = addLine(firstName, top: y + 8, type: .person)
        y = addLine("TO", top: y + 8, type: .small)
        y = addLine(lastName, top: y + 8, type: .person)
        y = addLine("IN", top: y + 8, type: .small)
        // Add steps
        let stepsSize:CGFloat = 140
        let stepsLabel = UILabel(frame: CGRect(x: (self.frame.width - stepsSize) / 2, y: y + 8, width: stepsSize, height: stepsSize))
        stepsLabel.font = UIFont(name: kFontName, size: 25)
        stepsLabel.text = "\(steps) \(steps == 1 ? "STEP" : "STEPS")"
        stepsLabel.textColor = UI.DarkColor()
        stepsLabel.textAlignment = NSTextAlignment.center
        stepsLabel.layer.backgroundColor = UIColor.white.cgColor
        stepsLabel.layer.cornerRadius = stepsSize / 2
        self.addSubview(stepsLabel)
        
        let exitButton = UIButton(frame:CGRect(x: 10, y: self.frame.height, width: self.frame.width/2 - 15, height: buttonHeight))
        exitButton.setTitle("MENU", for: UIControlState())
        exitButton.titleLabel?.font = UIFont(name: kFontBoldName, size: 17)
        exitButton.layer.cornerRadius = buttonHeight/2
        exitButton.backgroundColor = UI.MainBlue()
        exitButton.addTarget(self, action: #selector(WinnerView.exitPressed), for: UIControlEvents.touchUpInside)
        self.addSubview(exitButton)
      
        let againButton = UIButton(frame:CGRect(x: self.frame.width/2 + 5, y: self.frame.height, width: self.frame.width/2 - 15, height: buttonHeight))
        againButton.setTitle("AGAIN", for: UIControlState())
        againButton.titleLabel?.font = UIFont(name: kFontBoldName, size: 17)
        againButton.layer.cornerRadius = buttonHeight/2
        againButton.backgroundColor = UI.MainBlue()
        againButton.addTarget(self, action: #selector(WinnerView.againPressed), for: UIControlEvents.touchUpInside)
        self.addSubview(againButton)
        
        let targetY:CGFloat = self.frame.height - (buttonHeight + 10)
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: { 
            exitButton.frame = CGRect(x: exitButton.frame.origin.x, y: targetY, width: exitButton.frame.width, height: exitButton.frame.height)
            againButton.frame = CGRect(x: againButton.frame.origin.x, y: targetY, width: againButton.frame.width, height: againButton.frame.height)
        }, completion: nil)
    }
    
    func exitPressed(){
        if let delegate = self.delegate {
            if delegate.responds(to: #selector(PlayViewController.exitToMainMenu)) {
                delegate.perform(#selector(PlayViewController.exitToMainMenu))
            }
        }
    }
    
    func againPressed(){
        if let delegate = self.delegate {
            if delegate.responds(to: #selector(PlayViewController.retry)) {
                delegate.perform(#selector(PlayViewController.retry))
            }
        }
    }
}
