//
//  PlayViewController.swift
//  SixDegrees
//
//  Created by Alin Nomura on 1/15/16.
//  Copyright © 2016 Nomura. All rights reserved.
//

import UIKit

enum Entity {
    case person
    case media
    case none
}

let kMaxSteps:Int = 6

class PlayViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, EndPersonViewDelegate {

    var menuActionDelegate:MenuActionDelegate?
    var initialStartFrame:CGRect?
    var selectedEntity:Entity = Entity.none
    var personArray:[Person] = []
    var mediaArray:[Media] = []
    var selectedPersons:[Person] = []
    var selectedMedias:[Media] = []
    var step:Int = 0
    var numberOfPages = 0
    var message:MessageBox?
    var endPersonView:EndPersonView!
    var endPersonImage:UIImage?
    var shadow:UIView!
    
    @IBOutlet var topView: TopView!
    @IBOutlet var xButton: UIButton!
    @IBOutlet var targetButton: UIButton!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var refreshButton: UIButton!
    @IBOutlet var progressView: UIProgressView!
    
    @IBAction func xButtonTapped() {
        self.message = MessageBox()
        self.message!.title = "Quit"
        self.message!.message = "Are you sure you want to end this game?"
        self.message!.button1Title = "Quit"
        self.message!.button1Selector = #selector(PlayViewController.exitToMainMenu)
        self.message!.button1DoesDismiss = true
        self.message!.button2Title = "Retry"
        self.message!.button2Selector = #selector(PlayViewController.retry)
        self.message!.button2DoesDismiss = true
        self.message!.hasDismissButton = true
        self.message!.show(controller: self)
    }
    @IBAction func targetButtonTapped() {
        self.shadow.isUserInteractionEnabled = true
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: { 
            self.shadow!.backgroundColor = UIColor.black.withAlphaComponent(0.8)
            self.endPersonView.frame = CGRect(x: (self.view.frame.width - self.endPersonView.frame.width) / 2, y: self.endPersonView.frame.origin.y, width: self.endPersonView.frame.width, height: self.endPersonView.frame.height)
        }, completion: nil)
    }
    
    @IBAction func refreshButtonTapped() {
        if self.selectedEntity == Entity.media {
            self.fetchMediaCredits()
        } else {
            self.fetchPersonCredits()
        }
        self.refreshButton.isHidden = true
    }
    
    func hideEndPersonView() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.shadow?.backgroundColor = UIColor.clear
            self.endPersonView.frame = CGRect(x: self.view.frame.width, y: self.endPersonView.frame.origin.y, width: self.endPersonView.frame.width, height: self.endPersonView.frame.height)
        }, completion: { (finished:Bool) in
            self.shadow.isUserInteractionEnabled = false
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeView()
        self.initializeGame()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let initialFrame = self.initialStartFrame {
            let firstImageView:UIImageView = self.scrollView.viewWithTag(100 + self.numberOfPages) as! UIImageView
            let destinationFrame = firstImageView.frame
            // Get the frame passed in from the main menu and adjust the x value to make sure its properly centered
            let f = self.view.convert(initialFrame, to: self.scrollView)
            let x = (self.scrollView.frame.width - f.width) / 2
            let y = f.origin.y
            let w = f.width
            let h = f.height
            firstImageView.frame = CGRect(x: x, y: y, width: w, height: h)
            UIView.animate(withDuration: 0.5) {
                firstImageView.frame = destinationFrame
            }
        }
        // Create the EndPersonView and shadow
        self.shadow = UIView(frame: self.view.bounds)
        self.shadow.backgroundColor = UIColor.clear
        self.shadow.isUserInteractionEnabled = false
        self.view.addSubview(self.shadow)
        
        self.endPersonView = EndPersonView(frame:CGRect(x: self.view.frame.width, y: (self.view.frame.height - 333) / 2, width: 310, height: 333))
        self.endPersonView.delegate = self
        self.endPersonView.imageView.image = self.endPersonImage
        self.endPersonView.nameLabel.text = BL.getLastPerson()?.name
        self.view.addSubview(self.endPersonView)
    }
    
    func initializeView(){
        self.topView.scrollView = self.scrollView
        self.scrollView.delegate = self
        self.refreshButton.layer.cornerRadius = self.refreshButton.frame.height / 2
        self.refreshButton.isHidden = true
    }
 
    func initializeGame(){
        self.step = 0
        self.selectedPersons = []
        self.selectedMedias = []
        self.setupPager()
        self.progressView.progress = 0
        let firstPerson = BL.getFirstPerson()!
        BL.setSelectedPerson(firstPerson)
        self.selectedPersons.append(firstPerson)
        self.displayInfoForSelectedEntity()
        self.fetchPersonCredits()
        let firstImageView:UIImageView = self.scrollView.viewWithTag(100 + self.numberOfPages) as! UIImageView
        BL.addProfileImageWithPath(firstPerson.profilePath, imageView: firstImageView, activityIndicator: nil)
    }
    
    func setupPager(){
        for view in self.scrollView.subviews {
            view.removeFromSuperview()
        }
        self.scrollView.contentSize = CGSize(width: 0, height: self.scrollView.frame.size.height)
        self.scrollView.clipsToBounds = false
    }
    
    func expandScrollView(){
        self.scrollView.contentSize = CGSize(width: self.scrollView.contentSize.width + self.scrollView.frame.width, height: self.scrollView.contentSize.height)
        self.scrollView.setContentOffset(CGPoint(x: self.scrollView.contentSize.width - self.scrollView.frame.width, y: 0), animated: true)
        self.numberOfPages += 1
    }
    
    func addPageViews() {
        // Add label
        let nameLabel = UILabel(frame: CGRect(x: 0, y: self.scrollView.frame.height - 20, width: 0, height: 0))
        nameLabel.font = UIFont(name: kFontBoldName, size: 14)
        nameLabel.textColor = UI.LightColor()
        nameLabel.tag = 200 + self.numberOfPages
        self.scrollView.addSubview(nameLabel)
        
        // Add image
        let imageView = UIImageView(frame: CGRect(x: self.scrollView.contentSize.width - self.scrollView.frame.width, y: 0, width: self.scrollView.frame.width, height: self.scrollView.frame.height - 20))
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        imageView.clipsToBounds = true
        imageView.tag = 100 + self.numberOfPages
        self.scrollView.addSubview(imageView)
    }
    
    func resizeLastNameLabel(){
        if let nameLabel:UILabel = scrollView.viewWithTag(200 + self.numberOfPages) as? UILabel {
            nameLabel.sizeToFit()
            nameLabel.frame = CGRect(x: self.scrollView.contentSize.width - self.scrollView.frame.width/2 - nameLabel.frame.width/2, y: nameLabel.frame.origin.y, width: nameLabel.frame.width, height: 20)
        }
    }
    
    func exitToMainMenu(){
        if let menuActionDelegate = self.menuActionDelegate {
            menuActionDelegate.resetMenu()
        }
        self.dismiss(animated: true) {
            if let menuActionDelegate = self.menuActionDelegate {
                menuActionDelegate.considerAskingUserToRate()
            }
        }
    }
    
    func retry(){
        self.dismiss(animated: true) {
            if let menuActionDelegate = self.menuActionDelegate {
                menuActionDelegate.considerAskingUserToRate()
            }
        }
    }
    
    fileprivate func fetchPersonCredits(){
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: self.tableView.frame.height))
        activityIndicator.startAnimating()
        self.tableView.addSubview(activityIndicator)
        self.personArray = []
        self.tableView.reloadData()
        self.tableView.setContentOffset(CGPoint.zero, animated: true)
        BL.getPersonCredits() { (success:Bool, medias:[Media]?, errorMessage:String?) -> Void in
            activityIndicator.removeFromSuperview()
            if !success {
                var message = kInterwebsFailedMessage
                if let error = errorMessage {
                    message = "Server Error: \(error)"
                }
                let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: kOK, style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
                self.refreshButton.isHidden = false
            } else if let medias = medias {
                self.mediaArray = medias.sorted(by: { (media1:Media, media2:Media) -> Bool in
                    if let date1 = media1.date {
                        if let date2 = media2.date {
                            return date1.compare(date2 as Date) == ComparisonResult.orderedDescending
                        }
                        return false
                    }
                    return false
                })
            }
            self.tableView.reloadData()
        }
    }
    
    fileprivate func fetchMediaCredits(){
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: self.tableView.frame.height))
        activityIndicator.startAnimating()
        self.tableView.addSubview(activityIndicator)
        self.mediaArray = []
        self.tableView.reloadData()
        self.tableView.setContentOffset(CGPoint.zero, animated: true)
        BL.getMovieCredits() { (success:Bool, persons:[Person]?, errorMessage:String?) -> Void in
            activityIndicator.removeFromSuperview()
            if !success {
                var message = kInterwebsFailedMessage
                if let error = errorMessage {
                    message = "Server Error: \(error)"
                }
                let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: kOK, style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
                self.refreshButton.isHidden = false
            } else if let persons = persons {
                self.personArray = persons
            }
            self.tableView.reloadData()
        }
    }
    
    // Determines the info needed for display by checking which value is not nil in the BL
    // After determination is made the selectedEntity is set and the rest of the controls use that value to determine what to display
    fileprivate func displayInfoForSelectedEntity(){
        // Create a new imageView and name label in the scroll view
        self.expandScrollView()
        self.addPageViews()
        let lastImageView:UIImageView = self.scrollView.viewWithTag(100 + self.numberOfPages) as! UIImageView
        let lastLabel:UILabel = self.scrollView.viewWithTag(200 + self.numberOfPages) as! UILabel
        
        // Determine what entity is selected
        if let media:Media = BL.getSelectedMedia() {
            self.selectedEntity = Entity.media
            BL.addPosterImageWithPath(media.posterPath, imageView: lastImageView)
            lastLabel.text = media.title
        } else if let person:Person = BL.getSelectedPerson() {
            self.selectedEntity = Entity.person
            BL.addProfileImageWithPath(person.profilePath, imageView: lastImageView, activityIndicator: nil)
            lastLabel.text = person.name
        } else {
            self.selectedEntity = Entity.none
        }
        self.resizeLastNameLabel()
    }
    
    // MARK: Table View Data Source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.selectedEntity {
            case Entity.media: return self.personArray.count
            case Entity.person: return self.mediaArray.count
            default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.selectedEntity == Entity.media {
            let cellId = "PersonCell"
            let person:Person = self.personArray[indexPath.row]
            guard let cell:PersonTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellId) as? PersonTableViewCell else {
                let newCell = PersonTableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: cellId)
                newCell.actorName.text = person.name
                newCell.characterName.text = person.character
                return newCell
            }
            cell.actorName.text = person.name
            cell.characterName.text = person.character
            return cell
        } else if self.selectedEntity == Entity.person {
            let cellId = "MediaCell"
            let media:Media = self.mediaArray[indexPath.row]
            guard let cell:MediaTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellId) as? MediaTableViewCell else {
                let newCell = MediaTableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: cellId)
                newCell.titleLabel.text = media.title
                var year = UNKNOWN
                if let date = media.date {
                    let calendar = Calendar.current
                    let yearInt = (calendar as NSCalendar).component(NSCalendar.Unit.year, from: date as Date)
                    year = "\(yearInt)"
                }
                newCell.yearLabel.text = year
                return newCell
            }
            cell.titleLabel.text = media.title
            var year = UNKNOWN
            if let date = media.date {
                let calendar = Calendar.current
                let yearInt = (calendar as NSCalendar).component(NSCalendar.Unit.year, from: date as Date)
                year = "\(yearInt)"
            }
            cell.yearLabel.text = year
            return cell
        } else {
            return UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "default")
        }
    }
    
    // MARK: Table View Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
        if self.selectedEntity == Entity.media {
            let person = self.personArray[indexPath.row]
            self.selectedPersons.append(person)
            
            BL.setSelectedPerson(person)
            self.displayInfoForSelectedEntity()

            if person.id == BL.getLastPerson()?.id {
                self.win()
            } else if self.step >= kMaxSteps {
                self.lose()
            } else {
                self.fetchPersonCredits()
            }
        } else if self.selectedEntity == Entity.person {
            let media = self.mediaArray[indexPath.row]
            self.selectedMedias.append(media)
            self.incrementStep()
            BL.setSelectedMedia(media)
            self.displayInfoForSelectedEntity()
            self.fetchMediaCredits()
        }
    }
    
    // MARK: Scroll View Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let off = scrollView.contentOffset.x
        for view in scrollView.subviews {
            if let imageView = view as? UIImageView {
                let diff = max(0, 300 - abs(Int(imageView.frame.origin.x - off)))
                let scale:CGFloat = CGFloat(diff)/300
                let width = scrollView.frame.width
                let height = (scrollView.frame.height - 20) * scale
                imageView.frame = CGRect(x: imageView.frame.origin.x, y: (scrollView.frame.height - 20) - height, width: width, height: height)
                imageView.alpha = scale
            } else if let label = view as? UILabel {
                let left = (label.frame.origin.x + label.frame.width/2) - scrollView.frame.width/2
                let diff = max(0, 65 - abs(Int(left - off)))
                let alpha:CGFloat = CGFloat(diff)/65
                label.alpha = alpha
            }
        }
    }
    
    fileprivate func incrementStep(){
        self.step += 1
        let progress:Float = Float(self.step)/Float(kMaxSteps)
        let hueRange:CGFloat = 0.3 // Lower is worse (0 = Red) higher is better (0.3 = green)
        let hue:CGFloat = (1.0 - CGFloat(progress)) * hueRange
        let saturation:CGFloat = 1.0
        let brightness:CGFloat = 1.0
        self.progressView.setProgress(progress, animated: true)
        self.progressView.progressTintColor = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
    }
    
    fileprivate func win(){
        self.xButton.isHidden = true
        self.targetButton.isHidden = true
        self.tableView.isScrollEnabled = false
        let firstName = self.selectedPersons[0].name
        let lastName = self.selectedPersons[self.selectedPersons.count-1].name
        let steps = self.selectedMedias.count
        let winView = WinnerView(frame: self.tableView.frame, controller: self, firstName:firstName, lastName:lastName, steps: steps)
        self.view.addSubview(winView)
        BL.incrementNumberOfWins()
    }
    
    fileprivate func lose(){
        self.message = MessageBox()
        self.message!.title = "Game Over!"
        self.message!.message = "Better luck next time"
        self.message!.button1Title = "Menu"
        self.message!.button1Selector = #selector(PlayViewController.exitToMainMenu)
        self.message!.button1DoesDismiss = true
        self.message!.button2Title = "Retry"
        self.message!.button2Selector = #selector(PlayViewController.retry)
        self.message!.button2DoesDismiss = true
        self.message!.show(controller: self)
    }
}

class TopView:UIView {
    var scrollView:UIScrollView!
    // The following override allows touches on this view to control the scroll view.
    // This is done so that the scroll view pages can be thinner and allow the scroll view to be swiped from outside of it's frame
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        return self.point(inside: point, with: event) ? self.scrollView : nil
    }
}

// MARK: Table View Cells
class MediaTableViewCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var yearLabel: UILabel!
}

class PersonTableViewCell: UITableViewCell {
    @IBOutlet var actorName: UILabel!
    @IBOutlet var characterName: UILabel!
}
