//
//  MenuSceneView.swift
//  SixDegrees
//
//  Created by Alin Nomura on 8/19/17.
//  Copyright © 2017 Nomura. All rights reserved.
//

import SceneKit

extension SCNVector3 {
    var magnitude:SCNFloat {
        get {
            return sqrt((x*x)+(y*y)+(z*z))
        }
    }
}

class MenuScene: SCNView {
    
    let cameraAnchor = SCNNode()
    var sphereMaterial:SCNMaterial!
    var sphereGeometry:SCNSphere!
    var firstSphereNode:SCNNode?
    var lineElement:SCNGeometryElement!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    fileprivate func setupView() {
        self.preferredFramesPerSecond = 30
        self.autoenablesDefaultLighting = false
        self.backgroundColor = UIColor.clear
        self.scene = SCNScene()
        self.scene!.rootNode.addChildNode(self.cameraAnchor)
        
        let cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        cameraNode.position = SCNVector3(x: 0, y: -80, z: 300)
        cameraNode.camera?.zFar = 1000
        self.cameraAnchor.addChildNode(cameraNode)
        
        self.sphereMaterial = SCNMaterial()
        self.sphereMaterial.diffuse.contents = UI.MainDarkBlue()
        
        self.sphereGeometry = SCNSphere(radius: 1)
        self.sphereGeometry.segmentCount = 8
        self.sphereGeometry.firstMaterial = self.sphereMaterial
        
        let indecies:[Int32] = [0, 1]
        self.lineElement = SCNGeometryElement(indices: indecies, primitiveType: .line)
        
        self.createGraph()
        
        let rotation = CABasicAnimation(keyPath: "rotation")
        rotation.fromValue = SCNVector4(0, 1, 0, 0)
        rotation.toValue = SCNVector4(0, 1, 0, Float.pi * 2)
        rotation.duration = 30
        rotation.repeatCount = .infinity
        self.cameraAnchor.addAnimation(rotation, forKey: nil)
    }
    
    func reset() {
        self.destroyGraph()
        self.play(nil)
        self.createGraph()
    }
    
    func destroyGraph() {
        self.pause(nil)
        self.firstSphereNode?.removeFromParentNode()
    }

    let generations = [1, 2, 3, 3, 3, 3]
    
    private func createGraph() {
        self.firstSphereNode = SCNNode(geometry: self.sphereGeometry)
        self.firstSphereNode!.position = SCNVector3(0, 0, 0)
        self.scene!.rootNode.addChildNode(self.firstSphereNode!)
        self.makeChildren(generations.count - 1, parent: self.firstSphereNode!)
    }
    
    private func makeChildren(_ num:Int, parent:SCNNode) {
        if num > 0 {
            for _ in 1...generations[num] {
                let subNode = parent.copy() as! SCNNode
                subNode.position = self.randomPointFromNode(parent)
                subNode.scale = SCNVector3(0, 0, 0)
                parent.addChildNode(subNode)
                
                let lineSource = SCNGeometrySource(vertices: [SCNVector3(0, 0, 0), subNode.position])
                let lineGeo = SCNGeometry(sources: [lineSource], elements: [self.lineElement])
                let newLine = SCNNode(geometry: lineGeo)
                parent.addChildNode(newLine)
                
                let sphereAnimation = CABasicAnimation(keyPath: "scale")
                sphereAnimation.fromValue = SCNVector3(0, 0, 0)
                sphereAnimation.toValue = SCNVector3(1, 1, 1)
                sphereAnimation.duration = 0.5
                subNode.addAnimation(sphereAnimation, forKey: nil)
                subNode.scale = SCNVector3(1, 1, 1)
                
                self.makeChildren(num - 1, parent: subNode)
            }
        }
    }
    
    private func randomPointFromNode(_ node:SCNNode) -> SCNVector3 {
        let range:UInt32 = 20
        let halfRange:Float = 10
        let x:Float = Float(arc4random_uniform(range)) - halfRange + node.position.x
        let y:Float = Float(arc4random_uniform(range)) - halfRange + node.position.y
        let z:Float = Float(arc4random_uniform(range)) - halfRange + node.position.z
        return SCNVector3(x, y, z)
    }
}
